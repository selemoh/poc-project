package com.test.SuperAdmin;

import java.io.IOException;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.base.Base;
import com.pages.MasterConfigs.ChargerConfigs;
import com.pages.MasterConfigs.ConnectorConfigs;
import com.repo.pages.Global.LoginPage;
import com.utils.TestUtils;

public class MasterConfigsTest extends Base {
	static LoginPage lg = new LoginPage();
	static ChargerConfigs mcg = new ChargerConfigs();
	static ConnectorConfigs mcg1 = new ConnectorConfigs();
	static String path;
	
	@Test(priority=1)
	public void addChargerConfTest() throws IOException, InterruptedException {
		logger = extent.createTest("Charger Congiguration Test").assignCategory("Regression").assignAuthor("Mohit");
    	mcg.add_Edit_Delete_Charger();
	}
	
	@Test(priority=2)
	public void ConnectConfTest() throws IOException, InterruptedException {
		logger = extent.createTest("Connector Congiguration Test").assignCategory("Regression").assignAuthor("Mohit");
		logger.log(Status.INFO, "The Chrome browser and application is going to launch");
		Universalbrowser("chrome");
    	mcg1.add_Edit_Delete_Connector();
	}
	
	@AfterMethod
	public void finish(ITestResult result) throws InterruptedException, IOException {
		if(result.getStatus()==ITestResult.SUCCESS){
			logger.log(Status.PASS, MarkupHelper.createLabel(result.getName(), ExtentColor.GREEN));
		}
		else if(result.getStatus()==ITestResult.FAILURE) {
			logger.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" - This test case failed due to following issues: "
				    , ExtentColor.RED));
			path = TestUtils.getScreenshot(driver, result.getName());
			logger.log(Status.INFO, result.getThrowable());
			logger.fail("Screenshot attached above for failed TestCase").addScreenCaptureFromPath(path);
		}
		Thread.sleep(2000);
		driver.quit();
	}
}

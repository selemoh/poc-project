package com.test.SuperAdmin;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.base.Base;
import com.repo.pages.Global.LoginPage;

public class MiscTest extends Base{

	LoginPage lpg = new LoginPage();
	
	@Test
	public void disableElementstest() throws IOException, InterruptedException {
		Universalbrowser("firefox");
		Thread.sleep(1000);
		lpg.login3();
		drv.waitForAngularRequestsToFinish();
		WebDriverWait wait = new WebDriverWait(driver, 30);
		driver.findElement(By.xpath("/html/body/app-root/app-super-admin/mat-sidenav-container/mat-sidenav/div/app-sidebar/div/div[2]/mat-nav-list/mat-accordion[4]/mat-expansion-panel/mat-expansion-panel-header/span[1]/mat-panel-title/span")).click();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/app-root/app-super-admin/mat-sidenav-container/mat-sidenav/div/app-sidebar/div/div[2]/mat-nav-list/mat-accordion[4]/mat-expansion-panel/div/div/a[1]/div/span"))).click();
		String elem = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/app-root/app-super-admin/mat-sidenav-container/mat-sidenav-content/app-chargerconfiguration/mat-card[2]/form/div/table/tbody/tr[1]/td[2]/input"))).getAttribute("value");
		
		System.out.println("Value stored in input field is "+elem);
	}
	
		
}

package com.test.SuperAdmin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.commons.mail.EmailException;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.base.Base;
import com.pages.SuperAdmin.SuperAdminLogin;
import com.utils.TestUtils;

public class SuperAdminLoginTest extends Base {

	static SuperAdminLogin lg = new SuperAdminLogin();
	
	@BeforeTest
	public void setUp() throws IOException {
		reporter();
	}
	
	@Test(dataProvider="excelDataP", priority=1, enabled=true)
	public void Login_N_Logout(String data1Mail, String data1Pasw) throws IOException, InterruptedException {
		logger = extent.createTest("Login Function Test").assignCategory("Smoke").assignDevice("chrome");
		logger.log(Status.INFO,"This test case will verify the - Login_N_Logout function with positive test data.");
		logger.log(Status.INFO,"Calling 'Universalbrowser('chrome')' method from 'Base' class and passing the browser argument at run time");
		logger.log(Status.INFO,"This above step also includes the maximization of Browser, Deletion of Cookies, Page_Load_Time_Out and hitting the Application URL");
		Universalbrowser("chrome");
		logger.log(Status.INFO,"Calling 'login' method and passing the email and password arguments on runtime");
		logger.log(Status.INFO,"The above step also involves the Login_N_Logout steps as follows: ");
		lg.login(data1Mail, data1Pasw);
	}
	
	@DataProvider
	public Iterator<Object[]> excelDataP() throws IOException {
		ArrayList<Object[]> testData = TestUtils.getDatafrmExl();
		return testData.iterator();
	}
	
	
	@AfterMethod
	public void tearDown(ITestResult result) throws IOException {
		if(ITestResult.SUCCESS==result.getStatus())	{
	logger.log(Status.PASS, MarkupHelper.createLabel("The Test case "+result.getName()+" is passed", ExtentColor.GREEN));
	}
	else if(ITestResult.FAILURE==result.getStatus()) {
		String path = TestUtils.getScreenshot(driver, result.getName());
	logger.log(Status.FAIL, MarkupHelper.createLabel("The test case "+result.getName()+" is failed due to below reason", ExtentColor.RED)).addScreenCaptureFromPath(path);
	logger.log(Status.INFO, result.getThrowable());
	
	}
	driver.quit();
	
	}
	
	@AfterSuite
	public void generateReport() throws EmailException {
		extent.flush();
	//	TestUtils.sendMail();
	}
}

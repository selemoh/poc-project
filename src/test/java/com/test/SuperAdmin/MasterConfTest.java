package com.test.SuperAdmin;

import java.io.IOException;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.base.Base;
import com.pages.MasterConfigs.ChargerConfigs;
import com.pages.MasterConfigs.ConnectorConfigs;
import com.repo.pages.Global.LoginPage;
import com.utils.TestUtils;

public class MasterConfTest extends Base {
	static LoginPage lg = new LoginPage();
	static ChargerConfigs mcg = new ChargerConfigs();
	static ConnectorConfigs ccg = new ConnectorConfigs();
	static String path;
	
	
//	@BeforeTest
//	public void setUp() throws IOException {
//		reporter();
//	}
	
	@Test(priority=1)
	public void addChargerConfTest() throws IOException, InterruptedException {
		logger = extent.createTest("Master_Conf_Test_1").assignCategory("Regression").assignAuthor("Mohit");
		logger.log(Status.INFO, "The Chrome browser and application is going to launch");
		Universalbrowser("firefox");
		logger.log(Status.INFO, "The user is logged in with Super Admin credentials");
		lg.login2();
		Thread.sleep(2000);
		mcg.add_Edit_Delete_Charger();
		ccg.add_Edit_Delete_Connector();
	}
	
	/*
	 * @Test(priority=2) public void editChargerConfTest() throws IOException,
	 * InterruptedException { logger =
	 * extent.createTest("Master_Conf_Test_2").assignCategory("Regression").
	 * assignAuthor("Mohit"); logger.log(Status.INFO,
	 * "The Chrome browser and application is going to launch");
	 * Universalbrowser("chrome"); logger.log(Status.INFO,
	 * "The user is logged in with Super Admin credentials"); lg.login2();
	 * Thread.sleep(2000); mcg.edit_Charger_Type(); }
	 * 
	 * @Test(priority=3) public void deleteChargerConfTest() throws IOException,
	 * InterruptedException { logger =
	 * extent.createTest("Master_Conf_Test_3").assignCategory("Regression").
	 * assignAuthor("Mohit"); logger.log(Status.INFO,
	 * "The Chrome browser and application is going to launch");
	 * Universalbrowser("chrome"); logger.log(Status.INFO,
	 * "The user is logged in with Super Admin credentials"); lg.login2();
	 * Thread.sleep(2000); mcg.delete_Charger_Type(); }
	 */
	
	/*
	 * @Test(enabled=false) public void connectorConfTest() throws IOException,
	 * InterruptedException { logger =
	 * extent.createTest("Master_Conf_Tst_2").assignCategory("Regression");
	 * logger.log(Status.INFO,
	 * "The Chrome browser and application is going to launch");
	 * Universalbrowser("chrome"); logger.log(Status.INFO,
	 * "The user is logged in with Super Admin credentials"); lg.login2();
	 * Thread.sleep(2000); mcg.addN_Edit_Connector(); }
	 */
	
	@AfterMethod
	public void finish(ITestResult result) throws InterruptedException, IOException {
		if(result.getStatus()==ITestResult.SUCCESS){
			logger.log(Status.PASS, MarkupHelper.createLabel(result.getName(), ExtentColor.GREEN));
		}
		else if(result.getStatus()==ITestResult.FAILURE) {
			logger.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" - This test case failed due to following issues: "
				    , ExtentColor.RED));
			path = TestUtils.getScreenshot(driver, result.getName());
			logger.log(Status.INFO, result.getThrowable());
			logger.fail("Screenshot attached above for failed TestCase").addScreenCaptureFromPath(path);
		}
		
		Thread.sleep(2000);
		driver.quit();
	}
}

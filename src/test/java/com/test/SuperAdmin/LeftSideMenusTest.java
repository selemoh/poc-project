package com.test.SuperAdmin;

import java.io.IOException;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.base.Base;
import com.pages.SuperAdmin.LeftSideMenus;
import com.utils.TestUtils;

public class LeftSideMenusTest extends Base {
    
	LeftSideMenus lsm = new LeftSideMenus();
	
//    @BeforeMethod
//	void launch() throws IOException {
//		reporter();
//	}
	
	@Test
	void sideMenusTest() throws IOException, InterruptedException {
		logger = extent.createTest("Side_Menus_Page_Visit_test").assignCategory("Regression").assignAuthor("Mohit");
		logger.log(Status.INFO, "This test is to verify that the pages in Sub menus are loading");
		Universalbrowser("firefox");
		lsm.sideMenusSA();
	}
	
	@AfterMethod
	void tearDown(ITestResult result) throws IOException, InterruptedException {
	if(result.getStatus()==ITestResult.FAILURE) {
		String path = TestUtils.getScreenshot(driver, result.getName());
		logger.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+ " is failed due to below reason:", ExtentColor.RED)).addScreenCaptureFromPath(path);
		logger.log(Status.INFO, result.getThrowable());
	}
	else if(result.getStatus()==ITestResult.SUCCESS) {
		logger.log(Status.PASS, MarkupHelper.createLabel(result.getName()+ " is passed successfully", ExtentColor.GREEN));
	}
	Thread.sleep(2000);
	driver.quit();
	}
	
	/*
	 * @AfterSuite void generateReport() throws EmailException { extent.flush(); //
	 * TestUtils.sendMail(); }
	 */
}

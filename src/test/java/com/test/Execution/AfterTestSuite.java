package com.test.Execution;

import org.apache.commons.mail.EmailException;
import org.testng.annotations.AfterSuite;

import com.base.Base;
import com.utils.TestUtils;

public class AfterTestSuite extends Base{
	
	@AfterSuite
	public void shutDown() throws EmailException {
		extent.flush();
		TestUtils.sendMail();
	}

}

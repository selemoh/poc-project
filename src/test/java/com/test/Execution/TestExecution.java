package com.test.Execution;

import java.io.IOException;
import java.util.List;
import org.testng.TestNG;
import org.testng.collections.Lists;

public class TestExecution {

	public static void main(String [] args) throws IOException, InterruptedException {
		/* TestListenerAdapter tla = new TestListenerAdapter(); */
	    TestNG testng = new TestNG();
	    List<String> suites = Lists.newArrayList();
	    suites.add(System.getProperty("user.dir")+"\\testng.xml");
	    testng.setTestSuites(suites);
	    testng.run();
	}
}

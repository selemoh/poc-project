package com.test.CommonTests;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.base.Base;
import com.repo.pages.Global.ResetPassword;
import com.utils.TestUtils;

public class ResetPasswordTest extends Base{

	static ResetPassword rstPass = new ResetPassword();
	
	@BeforeTest
	public void setUp() throws IOException {
		reporter();
	}
	
	@DataProvider
	public Iterator<Object[]> excelDataP() throws IOException {
		ArrayList<Object[]> testData = TestUtils.getResetPassData();
		return testData.iterator();
	}
	
	@Test(dataProvider = "excelDataP", priority=1, enabled=true)
	public static void Forgot_Password_Reset_Email(String resetpass) throws IOException, InterruptedException {
		logger= extent.createTest("Forgot Paswword Test").assignCategory("Sanity");
		logger.info("This test case is to check the reset password function");
		logger.info("Basebrowser - "+"This steps shows us that AUT is getting launched followed by the browser launch.");
		Universalbrowser("firefox");
		logger.info("forgotPassword - "+"This steps shows us that steps to retrieve password are being performed");
		rstPass.forgotPassword(resetpass);
	}
	
	@AfterMethod
	public void finish(ITestResult result) throws InterruptedException, IOException {
		if(result.getStatus()==ITestResult.FAILURE) {
			String path = TestUtils.getScreenshot(driver, result.getName());
			logger.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" - failed, and Error is mentioned in next line. "+" ,Also the screenshot is attached above"
				    , ExtentColor.RED)).addScreenCaptureFromPath(path);
			logger.log(Status.INFO, result.getThrowable());

		}else if(result.getStatus()==ITestResult.SUCCESS){
			logger.log(Status.PASS, MarkupHelper.createLabel(result.getName()+ "Test Case is Passed", ExtentColor.GREEN));
		}
		
		Thread.sleep(2000);
		driver.quit();
	}
	
}

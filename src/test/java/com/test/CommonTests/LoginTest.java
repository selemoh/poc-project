package com.test.CommonTests;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.base.Base;
import com.repo.pages.Global.LoginPage;
import com.utils.TestUtils;

public class LoginTest extends Base{
	
	static LoginPage lg = new LoginPage();
//		static ReadExcel readd = new ReadExcel();
	
	@BeforeTest
	public void setUp() throws IOException {
		reporter();
	}
	
	@Test(dataProvider="excelDataP", priority=1, enabled=true)
	public void Login_N_Logout(String data1Mail, String data1Pasw) throws IOException, InterruptedException {
		logger = extent.createTest("Login Function Test").assignCategory("Smoke").assignDevice("chrome");
		logger.log(Status.INFO,"This test case will verify the - Login_N_Logout function with positive test data.");
		logger.log(Status.INFO,"Calling 'Universalbrowser('chrome')' method from 'Base' class and passing the browser argument at run time");
		logger.log(Status.INFO,"This above step also includes the maximization of Browser, Deletion of Cookies, Page_Load_Time_Out and hitting the Application URL");
		Universalbrowser("chrome");
		logger.log(Status.INFO,"Calling 'login' method and passing the email and password arguments on runtime");
		logger.log(Status.INFO,"The above step also involves the Login_N_Logout steps as follows: ");
		lg.login(data1Mail, data1Pasw);
	}	
	
	@DataProvider
	public Iterator<Object[]> excelDataP() throws IOException {
		ArrayList<Object[]> testData = TestUtils.getDatafrmExl();
		return testData.iterator();
	}
	
	@AfterMethod
	public void finish(ITestResult result) throws InterruptedException, IOException {
		if(result.getStatus()==ITestResult.FAILURE) {
			String path = TestUtils.getScreenshot(driver, result.getName());
			logger.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" - failed, and Error is mentioned in next line. "+" ,Also the screenshot is attached above"
				    , ExtentColor.RED)).addScreenCaptureFromPath(path);
			logger.log(Status.INFO, result.getThrowable());

		}else if(result.getStatus()==ITestResult.SUCCESS){
			logger.log(Status.PASS, MarkupHelper.createLabel(result.getName()+ "Test Case is Passed", ExtentColor.GREEN));
		}
		Thread.sleep(2000);
		driver.quit();
	}
	
	@AfterSuite
	public void generateReport() {
		extent.flush();
	}
}

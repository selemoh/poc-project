package com.repo.pages.Global;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.base.Base;
import com.config.TestConfigs;
import com.paulhammant.ngwebdriver.ByAngular;
import com.paulhammant.ngwebdriver.ByAngularButtonText;

public class ResetPassword extends Base {

	By linktext = By.xpath("//mat-card/div[1]/a");
	By emailid = By.xpath("//mat-card/form/div/input");
	ByAngularButtonText bttn = ByAngular.buttonText("Reset Password");
	
	public void forgotPassword(String emaild) throws InterruptedException {
		driver.navigate().refresh();
		drv.waitForAngularRequestsToFinish();
		driver.findElement(linktext).click();
		drv.waitForAngularRequestsToFinish();
		driver.findElement(emailid).clear();
		driver.findElement(emailid).sendKeys(emaild);
		driver.findElement(bttn).click();
		drv.waitForAngularRequestsToFinish();
		WebDriverWait wait = new WebDriverWait(driver,10);

		try {
			Assert.assertEquals(wait.until(ExpectedConditions.presenceOfElementLocated(emailid)).getAttribute("value"), TestConfigs.resetEmail);

		}catch(Exception e) {
			e.printStackTrace();
		}
		Thread.sleep(3000);
	}
}

package com.repo.pages.Global;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import com.base.Base;
import com.config.TestConfigs;
import com.paulhammant.ngwebdriver.ByAngular;
import com.paulhammant.ngwebdriver.ByAngularButtonText;

public class LoginPage extends Base {

	static File file;
	static FileInputStream fis;
	static XSSFWorkbook wb;
	static XSSFSheet sheets;
	public static String data1Mail=null;
	public static String data1Pasw = null;
	By email = By.xpath("//mat-card/form/div[1]/input");
	By pass = By.xpath("//mat-card/form/div[2]/div[2]/input");		
	ByAngularButtonText btn = ByAngular.buttonText("Login");
	By alertpop = By.xpath("//app-snackbar/div/div/div");
	By alerttext = By.xpath("//snack-bar-container/app-snackbar/div/div[1]/div[2]");
	By userRole = By.xpath("//*[@class='org' or @text='Super Admin']");
	By fpassFailuremsg = By.xpath("//*[text()='This email is not a registered email ']");
	By fpassSuccessmsg = By.xpath("//app-snackbar/div/div/div[2]");
	By userPanel = By.xpath("//mat-sidenav-container/mat-sidenav-content/iw-top-bar/mat-toolbar/button[3]");
	By logout_lnk = By.xpath("//div/button[3]/span");
	By loginForm = By.xpath("//*[text()='Login to your account']");
	By loginForm1 = By.xpath("//mat-card/h6/span");

	public void login(String data1Mail, String data1Pasw) throws InterruptedException, IOException {

		logger.log(Status.INFO, "1 This step - driver.findElement(email).clear() - finds and clears the 'email' input field.");
		driver.findElement(email).clear();
		logger.log(Status.INFO, "2 This step -  driver.findElement(email).sendKeys(mail) enters the registered 'email' text into the input box");
		driver.findElement(email).sendKeys(data1Mail);
		logger.log(Status.INFO, "3 This step - driver.findElement(pass).clear() finds and clears the 'password' input field");
		driver.findElement(pass).clear();
		logger.log(Status.INFO, "4 This step - driver.findElement(pass).sendKeys(pasw) enters the valid 'password' text into the input box");
		driver.findElement(pass).sendKeys(data1Pasw);
		logger.log(Status.INFO, "5 This step - driver.findElement(btn).click() -clicks on 'Login' button");
		driver.findElement(btn).click();
		drv.waitForAngularRequestsToFinish();
		logger.log(Status.INFO, "6 After Clicking at 'Login' the script waits for the Dashboard page to appear with using - drv.waitForAngularRequestsToFinish() - step");
		WebDriverWait wait = new WebDriverWait(driver,30);
		logger.log(Status.INFO, "7 After Successful loading of the Dashboard, an assertion - Assert.assertEquals('','') verifies that user role is correct or not");

		Assert.assertEquals(wait.until(ExpectedConditions.presenceOfElementLocated(userRole)).getText(), "Super Admin", "Incorrect User account");
		logger.log(Status.INFO, "User is - "+ driver.findElement(userRole).getText());

		logger.log(Status.INFO, "8 This step - driver.findElement(userPanel).click() will now click on 'userOptions' menu list from top right");
		driver.findElement(userPanel).click();
		Thread.sleep(2000);
		logger.log(Status.INFO, "9 This step - driver.findElement(logout_lnk).click() clicks on 'logout' label/link");
		driver.findElement(logout_lnk).click();
		logger.log(Status.INFO, "10 This step - drv.waitForAngularRequestsToFinish() waits for login page to appear");
		drv.waitForAngularRequestsToFinish();
		logger.log(Status.INFO, "11 This step - Assert.assertEquals('','') verifies that login page appeared");		
		if(wait.until(ExpectedConditions.presenceOfElementLocated(email)).isDisplayed()) {
			logger.log(Status.INFO, "Login Page is visible");
		}else {
			logger.log(Status.INFO, "Login Page is not visible");
		}
		Thread.sleep(1000);
	}

	public void login2() throws InterruptedException, IOException {

		file = new File(TestConfigs.firsttestData);
		fis = new FileInputStream(file);
		wb = new XSSFWorkbook(fis);
		sheets = wb.getSheetAt(0);

		String dataMail = sheets.getRow(0).getCell(0).getStringCellValue();

		String dataPasw = sheets.getRow(0).getCell(1).getRawValue();	

		logger.log(Status.INFO, "1 This step - driver.findElement(email).clear() - finds and clears the 'email' input field.");
		driver.findElement(email).clear();
		logger.log(Status.INFO, "2 This step -  driver.findElement(email).sendKeys(mail) enters the registered 'email' text into the input box");
		driver.findElement(email).sendKeys(dataMail);
		logger.log(Status.INFO, "3 This step - driver.findElement(pass).clear() finds and clears the 'password' input field");
		driver.findElement(pass).clear();
		logger.log(Status.INFO, "4 This step - driver.findElement(pass).sendKeys(pasw) enters the valid 'password' text into the input box");
		driver.findElement(pass).sendKeys(dataPasw);
		logger.log(Status.INFO, "5 This step - driver.findElement(btn).click() -clicks on 'Login' button");
		driver.findElement(btn).click();
		drv.waitForAngularRequestsToFinish();
		logger.log(Status.INFO, "6 After Clicking at 'Login' the script waits for the Dashboard page to appear with using - drv.waitForAngularRequestsToFinish() - step");
		WebDriverWait wait = new WebDriverWait(driver,30);
		logger.log(Status.INFO, "7 After Successful loading of the Dashboard, an assertion - Assert.assertEquals('','') verifies that user role is correct or not");
		Assert.assertEquals(wait.until(ExpectedConditions.presenceOfElementLocated(userRole)).getText(), "Super Admin", "Incorrect User account");
	}
	
	public void login3() throws InterruptedException, IOException {

		file = new File(TestConfigs.firsttestData);
		fis = new FileInputStream(file);
		wb = new XSSFWorkbook(fis);
		sheets = wb.getSheetAt(0);

		String dataMail = sheets.getRow(0).getCell(0).getStringCellValue();

		String dataPasw = sheets.getRow(0).getCell(1).getRawValue();	

		
		driver.findElement(email).clear();
	
		driver.findElement(email).sendKeys(dataMail);
		
		driver.findElement(pass).clear();
		
		driver.findElement(pass).sendKeys(dataPasw);

		driver.findElement(btn).click();
		drv.waitForAngularRequestsToFinish();
		
		WebDriverWait wait = new WebDriverWait(driver,30);
		
		Assert.assertEquals(wait.until(ExpectedConditions.presenceOfElementLocated(userRole)).getText(), "Super Admin", "Incorrect User account");
	}
	
	
	public void login4() throws InterruptedException, IOException {
		
		WebDriverWait wait = new WebDriverWait(driver,30);
		logger.log(Status.INFO, "8 This step - driver.findElement(userPanel).click() will now click on 'userOptions' menu list from top right");
		driver.findElement(userPanel).click();
		Thread.sleep(2000);
		logger.log(Status.INFO, "9 This step - driver.findElement(logout_lnk).click() clicks on 'logout' label/link");
		driver.findElement(logout_lnk).click();
		logger.log(Status.INFO, "10 This step - drv.waitForAngularRequestsToFinish() waits for login page to appear");
		drv.waitForAngularRequestsToFinish();
		logger.log(Status.INFO, "11 This step - Assert.assertEquals('','') verifies that login page appeared");		
		if(wait.until(ExpectedConditions.presenceOfElementLocated(email)).isDisplayed()) {
			logger.log(Status.INFO, "Login Page is visible");
		}else {
			logger.log(Status.INFO, "Login Page is not visible");
		}
		drv.waitForAngularRequestsToFinish();
		Thread.sleep(3000);
	}
}

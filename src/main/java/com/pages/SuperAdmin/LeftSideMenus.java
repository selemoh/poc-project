package com.pages.SuperAdmin;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.base.Base;
import com.repo.pages.Global.LoginPage;

public class LeftSideMenus extends Base {
	
	LoginPage lp = new LoginPage();
	
	By dash = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-dashboard/breadcrum/ol");
	By expand = By.xpath("//mat-sidenav-container/mat-sidenav-content/iw-top-bar/mat-toolbar/button[1]/span/mat-icon");
	By TotalAvailConnectorWidget = By.xpath("/html/body/app-root/app-super-admin/mat-sidenav-container/mat-sidenav-content/app-dashboard/div/mat-card[1]/div[2]/div[2]/app-widget/div/div[2]/div/div[1]");
	By licenseBuy = By.xpath("//mat-sidenav-container/mat-sidenav/div/app-sidebar/div/div[2]/mat-nav-list/a[2]/div");
	By subsidiaries = By.xpath("//a[3]/div");
	By subscription = By.xpath("//mat-sidenav-container/mat-sidenav/div/app-sidebar/div/div[2]/mat-nav-list/a[4]/div");
	By subsManage = By.xpath("//mat-sidenav-container/mat-sidenav/div/app-sidebar/div/div[2]/mat-nav-list/a[5]");
	By reviewManage = By.xpath("//mat-sidenav-container/mat-sidenav/div/app-sidebar/div/div[2]/mat-nav-list/a[6]/div");
	By adminManage = By.xpath("//*[@id=\"mat-expansion-panel-header-0\"]/span[1]/mat-panel-title/span");
	By inviteAdm = By.xpath("//*[@id=\"cdk-accordion-child-0\"]/div/a[1]/div/span");
	By admRoleMng = By.xpath("//*[@id=\"cdk-accordion-child-0\"]/div/a[2]/div/span");
	By disbReqsts = By.xpath("//*[@id=\"cdk-accordion-child-0\"]/div/a[3]/div/span");
	By Delegate = By.xpath("//*[@id=\"cdk-accordion-child-0\"]/div/a[4]/div/span");
	By planManage = By.xpath("//*[@id=\"mat-expansion-panel-header-1\"]/span[1]/mat-panel-title/span");
	By managePlans = By.xpath("//*[@id=\"cdk-accordion-child-1\"]/div/a[1]/div/span");
	By tagPlans = By.xpath("//*[@id=\"cdk-accordion-child-1\"]/div/a[2]/div/span");
	By occp = By.xpath("//*[@id=\"mat-expansion-panel-header-2\"]/span[1]/mat-panel-title/span");
	By nearByMap = By.xpath("//*[@id=\"cdk-accordion-child-2\"]/div/a[1]/div/span");
	By charProfile = By.xpath("//*[@id=\"cdk-accordion-child-2\"]/div/a[2]/div/span");
	By charConfig = By.xpath("//*[@id=\"cdk-accordion-child-2\"]/div/a[3]/div/span");
	By registeredPoints = By.xpath("//*[@id=\"cdk-accordion-child-2\"]/div/a[4]/div/span");
	By FwUpdate = By.xpath("//*[@id=\"cdk-accordion-child-2\"]/div/a[5]/div/span");
	By masterConfigs = By.xpath("//mat-expansion-panel-header[@id='mat-expansion-panel-header-3']/span/mat-panel-title/span");
	By chargerConfs = By.xpath("//mat-sidenav-container/mat-sidenav/div/app-sidebar/div/div[2]/mat-nav-list/mat-accordion[4]/mat-expansion-panel/div/div/a[1]/div/span");
	By connectConfs = By.xpath("//mat-sidenav-container/mat-sidenav/div/app-sidebar/div/div[2]/mat-nav-list/mat-accordion[4]/mat-expansion-panel/div/div/a[2]/div/span");
	By tariffCons = By.xpath("//mat-sidenav-container/mat-sidenav/div/app-sidebar/div/div[2]/mat-nav-list/mat-accordion[4]/mat-expansion-panel/div/div/a[3]/div/span");
	By vehicleDetails = By.xpath("//*[@id=\"cdk-accordion-child-3\"]/div/a[4]/div/span");
	By slotBook = By.xpath("//*[@id=\"cdk-accordion-child-3\"]/div/a[5]/div/span");
	By BusinessEnt = By.xpath("//*[@id=\"mat-expansion-panel-header-4\"]/span[1]/mat-panel-title/span");
	By newInvite = By.xpath("//*[@id=\"cdk-accordion-child-4\"]/div/a[1]/div/span");
	By newApprove = By.xpath("//*[@id=\"cdk-accordion-child-4\"]/div/a[2]/div/span");
	By newLinkRqsts = By.xpath("//*[@id=\"cdk-accordion-child-4\"]/div/a[3]/div/span");
	By setCharBhel = By.xpath("//mat-sidenav-container/mat-sidenav/div/app-sidebar/div/div[2]/mat-nav-list/a[7]/div");
	By reports = By.xpath("//mat-sidenav-container/mat-sidenav/div/app-sidebar/div/div[2]/mat-nav-list/a[8]/div");
	By tariffManage = By.xpath("//*[@id=\"mat-expansion-panel-header-5\"]/span[1]/mat-panel-title/span");
	By chargingEndUser = By.xpath("//*[@id=\"cdk-accordion-child-5\"]/div/a[1]/div/span");
	By swappingEndUser = By.xpath("//*[@id=\"cdk-accordion-child-5\"]/div/a[2]/div/span");
	By chargingStation = By.xpath("//*[@id=\"cdk-accordion-child-5\"]/div/a[3]/div/span");
	By discomPrice = By.xpath("//*[@id=\"cdk-accordion-child-5\"]/div/a[4]/div/span");
	By tariffRecords = By.xpath("//*[@id=\"cdk-accordion-child-5\"]/div/a[5]/div/span");
	By rfid = By.xpath("//mat-sidenav-container/mat-sidenav/div/app-sidebar/div/div[2]/mat-nav-list/a[9]/div");
	By invoices = By.xpath("//mat-sidenav-container/mat-sidenav/div/app-sidebar/div/div[2]/mat-nav-list/a[10]/div");
	By trackNtrace = By.xpath("//mat-sidenav-container/mat-sidenav/div/app-sidebar/div/div[2]/mat-nav-list/a[11]/div");
	By blockChain = By.xpath("//mat-sidenav-container/mat-sidenav/div/app-sidebar/div/div[2]/mat-nav-list/a[12]/div");
	
	public void sideMenusSA() throws InterruptedException, IOException {
		lp.login2();
		drv.waitForAngularRequestsToFinish();
		WebDriverWait wait = new WebDriverWait(driver, 30);
		Thread.sleep(1000);
		Dimension one = driver.findElement(dash).getSize();
		System.out.println(one);
		driver.findElement(expand).click();
		Thread.sleep(1000);
		Dimension two = driver.findElement(dash).getSize();
		System.out.println(two);
		Assert.assertEquals(driver.findElement(dash).getSize(), two);
		driver.findElement(expand).click();
		Thread.sleep(1000);
		if(driver.findElement(TotalAvailConnectorWidget).isDisplayed()==true) {
			driver.findElement(licenseBuy).click();
		}		
		Thread.sleep(1000);
		wait.until(ExpectedConditions.presenceOfElementLocated(subsidiaries)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(subscription)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(subsManage)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(reviewManage)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(adminManage)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(inviteAdm)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(admRoleMng)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(disbReqsts)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(Delegate)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(adminManage)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(planManage)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(managePlans)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(tagPlans)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(planManage)).click();
		Thread.sleep(1000);
		wait.until(ExpectedConditions.presenceOfElementLocated(occp)).click();
		Thread.sleep(1000);
		wait.until(ExpectedConditions.presenceOfElementLocated(nearByMap)).click();
		Thread.sleep(3000);
		wait.until(ExpectedConditions.presenceOfElementLocated(charProfile)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(charConfig)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(registeredPoints)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(FwUpdate)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(occp)).click();
		/*
		 * Actions act = new Actions(driver);
		 * act.sendKeys(Keys.PAGE_DOWN).build().perform(); //act.sendKeys(target, keys)
		 */
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(masterConfigs)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(chargerConfs)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(connectConfs)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(tariffCons)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(vehicleDetails)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(slotBook)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(masterConfigs)).click();
		Thread.sleep(1000);
		wait.until(ExpectedConditions.presenceOfElementLocated(BusinessEnt)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(newInvite)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(newApprove)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(newLinkRqsts)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(BusinessEnt)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(setCharBhel)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(reports)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(tariffManage)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(chargingEndUser)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(swappingEndUser)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(chargingStation)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(discomPrice)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(tariffRecords)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(tariffManage)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(rfid)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(invoices)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(trackNtrace)).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.presenceOfElementLocated(blockChain)).click();
		Thread.sleep(4000);
		driver.navigate().back();
		Thread.sleep(3000);
		if (driver.findElement(masterConfigs).isDisplayed()) {
		wait.until(ExpectedConditions.presenceOfElementLocated(masterConfigs)).click();	
	}
	}
}
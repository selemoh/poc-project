package com.pages.MasterConfigs;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.Status;
import com.base.Base;
import com.config.ExcelReader;
import com.repo.pages.Global.LoginPage;

public class ConnectorConfigs extends Base{

	static ExcelReader read = new ExcelReader();
	static String inputText1;
	static String inputText2;
	static String inputText3;
	static String inputText4;
	static String inputText5;
	static String connectName = "TESTLAR";
	static String delConnect = "CHADEMOS";
	static List<WebElement> rows;
	static int rowSize;
	static Actions act;
	String rowOne;
	String rowTwo;
	String rowThree;
	String rowFour;
	String rowFive;


	// Elements of the page to be clicked or get inputs
	// ==============================================================      Connector Configuration - Add new Connectors
	By MConfig = By.xpath("//*[text()=' Master Configuration ']");
	By connetConfig = By.xpath("//*[text()=' Connector Configuration ']");
	By addConctType = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-connectorconfiguration/mat-card[1]/form/div/div/div[1]/input");
	By addChargCapcty = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-connectorconfiguration/mat-card[1]/form/div/div/div[2]/input");
	By chrgCapctyUnit = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-connectorconfiguration/mat-card[1]/form/div/div/div[3]/select");
	By selectCapctyUnit = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-connectorconfiguration/mat-card[1]/form/div/div/div[3]/select/option[2]");
	By sbmtBtn1 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-connectorconfiguration/mat-card[1]/div[2]/button[2]");

	// ==============================================================    Connector Configuration List - Edit Connectors
	By connecttable = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-connectorconfiguration/mat-card[2]/form/div/table/tbody/tr");
	By tableName = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-connectorconfiguration/mat-card[2]/div[1]/span");
	By actionIconB1 = By.xpath("//mat-sidenav-content/app-connectorconfiguration/mat-card[2]/form/div/table/tbody/tr[1]/td[4]/app-moredetaibtn/button/span/mat-icon");
	By actionIconB2 = By.xpath("//mat-sidenav-content/app-connectorconfiguration/mat-card[2]/form/div/table/tbody/tr[2]/td[4]/app-moredetaibtn/button/span/mat-icon");
	By actionIconB3 = By.xpath("//mat-sidenav-content/app-connectorconfiguration/mat-card[2]/form/div/table/tbody/tr[3]/td[4]/app-moredetaibtn/button/span/mat-icon");
	By actionIconB4 = By.xpath("//mat-sidenav-content/app-connectorconfiguration/mat-card[2]/form/div/table/tbody/tr[4]/td[4]/app-moredetaibtn/button/span/mat-icon");
	By actionIconB5 = By.xpath("//mat-sidenav-content/app-connectorconfiguration/mat-card[2]/form/div/table/tbody/tr[5]/td[4]/app-moredetaibtn/button/span/mat-icon");
	By editBtnB1 = By.xpath("//*[@id=\"cdk-overlay-2\"]/div/div/button[1]/span");
	By delBtnB1 = By.xpath("//*[@id=\"cdk-overlay-2\"]/div/div/button[2]/span");
	By editSave1 = By.xpath("//mat-sidenav-content/app-connectorconfiguration/mat-card[2]/form/div/table/tbody/tr[1]/td[4]/div/button");
	By editSave2 = By.xpath("//mat-sidenav-content/app-connectorconfiguration/mat-card[2]/form/div/table/tbody/tr[2]/td[4]/div/button");
	By editSave3 = By.xpath("//mat-sidenav-content/app-connectorconfiguration/mat-card[2]/form/div/table/tbody/tr[3]/td[4]/div/button");
	By editSave4 = By.xpath("//mat-sidenav-content/app-connectorconfiguration/mat-card[2]/form/div/table/tbody/tr[4]/td[4]/div/button");
	By editSave5 = By.xpath("//mat-sidenav-content/app-connectorconfiguration/mat-card[2]/form/div/table/tbody/tr[5]/td[4]/div/button");
	By editCancel1 = By.xpath("//mat-sidenav-content/app-connectorconfiguration/mat-card[2]/form/div/table/tbody/tr[1]/td[4]/div/i");
	By editCancel2 = By.xpath("//mat-sidenav-content/app-connectorconfiguration/mat-card[2]/form/div/table/tbody/tr[2]/td[4]/div/i");
	By editCancel3 = By.xpath("//mat-sidenav-content/app-connectorconfiguration/mat-card[2]/form/div/table/tbody/tr[3]/td[4]/div/i");
	By editCancel4 = By.xpath("//mat-sidenav-content/app-connectorconfiguration/mat-card[2]/form/div/table/tbody/tr[4]/td[4]/div/i");
	By editCancel5 = By.xpath("//mat-sidenav-content/app-connectorconfiguration/mat-card[2]/form/div/table/tbody/tr[5]/td[4]/div/i");
	By editInputB1 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-connectorconfiguration/mat-card[2]/form/div/table/tbody/tr[1]/td[2]/input");
	By editInputB2 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-connectorconfiguration/mat-card[2]/form/div/table/tbody/tr[2]/td[2]/input");
	By editInputB3 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-connectorconfiguration/mat-card[2]/form/div/table/tbody/tr[3]/td[2]/input");
	By editInputB4 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-connectorconfiguration/mat-card[2]/form/div/table/tbody/tr[4]/td[2]/input");
	By editInputB5 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-connectorconfiguration/mat-card[2]/form/div/table/tbody/tr[5]/td[2]/input");
	By editCaps1 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-connectorconfiguration/mat-card[2]/form/div/table/tbody/tr[1]/td[3]/div/input");
	By editCaps2 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-connectorconfiguration/mat-card[2]/form/div/table/tbody/tr[2]/td[3]/div/input");
	By editCaps3 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-connectorconfiguration/mat-card[2]/form/div/table/tbody/tr[3]/td[3]/div/input");
	By editCaps4 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-connectorconfiguration/mat-card[2]/form/div/table/tbody/tr[4]/td[3]/div/input");
	By editCaps5 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-connectorconfiguration/mat-card[2]/form/div/table/tbody/tr[5]/td[3]/div/input");
	By editCapsunit1 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-connectorconfiguration/mat-card[2]/form/div/table/tbody/tr[1]/td[3]/div/div/select");
	By editCapsunit2 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-connectorconfiguration/mat-card[2]/form/div/table/tbody/tr[2]/td[3]/div/div/select");
	By editCapsunit3 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-connectorconfiguration/mat-card[2]/form/div/table/tbody/tr[3]/td[3]/div/div/select");
	By editCapsunit4 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-connectorconfiguration/mat-card[2]/form/div/table/tbody/tr[4]/td[3]/div/div/select");
	By editCapsunit5 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-connectorconfiguration/mat-card[2]/form/div/table/tbody/tr[5]/td[3]/div/div/select");	

	By SbmtBtn1 = By.xpath("//*[text()='Submit']");
	By addMore = By.xpath("//*[text()='Add More']");
	By charCol1 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-chargerconfiguration/mat-card[2]/form/div/table/tbody/tr[1]/td[1]");
	By charCol2 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-chargerconfiguration/mat-card[2]/form/div/table/tbody/tr[2]/td[1]");
	By charCol3 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-chargerconfiguration/mat-card[2]/form/div/table/tbody/tr[3]/td[1]");
	By charCol4 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-chargerconfiguration/mat-card[2]/form/div/table/tbody/tr[4]/td[1]");
	By charCol5 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-chargerconfiguration/mat-card[2]/form/div/table/tbody/tr[5]/td[1]");
	// =====================================  Action Icon table row wise ============================================================================	
	By actionIcon1 = By.xpath("//mat-sidenav-content/app-connectorconfiguration/mat-card[2]/form/div/table/tbody/tr[1]/td[4]/app-moredetaibtn/button/span/mat-icon");
	By actionIcon2 = By.xpath("//mat-sidenav-content/app-connectorconfiguration/mat-card[2]/form/div/table/tbody/tr[2]/td[4]/app-moredetaibtn/button/span/mat-icon");
	By actionIcon3 = By.xpath("//mat-sidenav-content/app-connectorconfiguration/mat-card[2]/form/div/table/tbody/tr[3]/td[4]/app-moredetaibtn/button/span/mat-icon");
	By actionIcon4 = By.xpath("//mat-sidenav-content/app-connectorconfiguration/mat-card[2]/form/div/table/tbody/tr[4]/td[4]/app-moredetaibtn/button/span/mat-icon");
	By actionIcon5 = By.xpath("//mat-sidenav-content/app-connectorconfiguration/mat-card[2]/form/div/table/tbody/tr[5]/td[4]/app-moredetaibtn/button/span/mat-icon");
	// =================================== Edit action button =================================================	
	By editBtn1 = By.xpath("//*[text()='Edit']");
	By editBtn2 = By.xpath("//*[text()='Edit']");
	By editBtn3 = By.xpath("//*[text()='Edit']");
	By editBtn4 = By.xpath("//*[text()='Edit']");
	By editBtn5 = By.xpath("//*[text()='Edit']");
	//  ==================================  Edit Connector Input field 1 ================================================
	By edtInput1 = By.cssSelector("body > app-root > app-super-admin > mat-sidenav-container > mat-sidenav-content > app-connectorconfiguration > mat-card.mx-3.mt-3.all-page-margin-bottom.p-0.mat-card > form > div > table > tbody > tr:nth-child(1) > td:nth-child(2) > input");
	By edtInput2 = By.cssSelector("body > app-root > app-super-admin > mat-sidenav-container > mat-sidenav-content > app-connectorconfiguration > mat-card.mx-3.mt-3.all-page-margin-bottom.p-0.mat-card > form > div > table > tbody > tr:nth-child(2) > td:nth-child(2) > input");
	By edtInput3 = By.cssSelector("body > app-root > app-super-admin > mat-sidenav-container > mat-sidenav-content > app-connectorconfiguration > mat-card.mx-3.mt-3.all-page-margin-bottom.p-0.mat-card > form > div > table > tbody > tr:nth-child(3) > td:nth-child(2) > input");
	By edtInput4 = By.cssSelector("body > app-root > app-super-admin > mat-sidenav-container > mat-sidenav-content > app-connectorconfiguration > mat-card.mx-3.mt-3.all-page-margin-bottom.p-0.mat-card > form > div > table > tbody > tr:nth-child(4) > td:nth-child(2) > input");
	By edtInput5 = By.cssSelector("body > app-root > app-super-admin > mat-sidenav-container > mat-sidenav-content > app-connectorconfiguration > mat-card.mx-3.mt-3.all-page-margin-bottom.p-0.mat-card > form > div > table > tbody > tr:nth-child(5) > td:nth-child(2) > input");
	// ==================================== Edit Capacity Input field 2  =====================================================	
	By edInput1 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-connectorconfiguration/mat-card[2]/form/div/table/tbody/tr[1]/td[3]/div/input");
	By edInput2 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-connectorconfiguration/mat-card[2]/form/div/table/tbody/tr[2]/td[3]/div/input");
	By edInput3 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-connectorconfiguration/mat-card[2]/form/div/table/tbody/tr[3]/td[3]/div/input");
	By edInput4 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-connectorconfiguration/mat-card[2]/form/div/table/tbody/tr[4]/td[3]/div/input");
	By edInput5 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-connectorconfiguration/mat-card[2]/form/div/table/tbody/tr[5]/td[3]/div/input");
	// ==================================== Edit Save Button ===========================================================================
	By sveBtn1 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-connectorconfiguration/mat-card[2]/form/div/table/tbody/tr[1]/td[4]/div/button");
	By sveBtn2 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-connectorconfiguration/mat-card[2]/form/div/table/tbody/tr[2]/td[4]/div/button");
	By sveBtn3 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-connectorconfiguration/mat-card[2]/form/div/table/tbody/tr[3]/td[4]/div/button");
	By sveBtn4 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-connectorconfiguration/mat-card[2]/form/div/table/tbody/tr[4]/td[4]/div/button");
	By sveBtn5 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-connectorconfiguration/mat-card[2]/form/div/table/tbody/tr[5]/td[4]/div/button");
	//  ======================================  Edit Cancel Btn =====================================================================
	By cnclBtn1 = By.cssSelector("mat-sidenav-container > mat-sidenav-content > app-connectorconfiguration > mat-card.mx-3.mt-3.all-page-margin-bottom.p-0.mat-card > form > div > table > tbody > tr:nth-child(1) > td:nth-child(4) > div > i");
	By cnclBtn2 = By.cssSelector("mat-sidenav-container > mat-sidenav-content > app-connectorconfiguration > mat-card.mx-3.mt-3.all-page-margin-bottom.p-0.mat-card > form > div > table > tbody > tr:nth-child(2) > td:nth-child(4) > div > i");
	By cnclBtn3 = By.cssSelector("mat-sidenav-container > mat-sidenav-content > app-connectorconfiguration > mat-card.mx-3.mt-3.all-page-margin-bottom.p-0.mat-card > form > div > table > tbody > tr:nth-child(3) > td:nth-child(4) > div > i");
	By cnclBtn4 = By.cssSelector("mat-sidenav-container > mat-sidenav-content > app-connectorconfiguration > mat-card.mx-3.mt-3.all-page-margin-bottom.p-0.mat-card > form > div > table > tbody > tr:nth-child(4) > td:nth-child(4) > div > i");
	By cnclBtn5 = By.cssSelector("mat-sidenav-container > mat-sidenav-content > app-connectorconfiguration > mat-card.mx-3.mt-3.all-page-margin-bottom.p-0.mat-card > form > div > table > tbody > tr:nth-child(5) > td:nth-child(4) > div > i");
	// ========================================= Action Delete button ===================================================================
	By delbtn1 = By.xpath("/html/body/div[3]/div[2]/div/div/div/button[2]");
	By delbtn2 = By.xpath("/html/body/div[3]/div[2]/div/div/div/button[2]");
	By delbtn3 = By.xpath("/html/body/div[3]/div[2]/div/div/div/button[2]");
	By delbtn4 = By.xpath("/html/body/div[3]/div[2]/div/div/div/button[2]");
	By delbtn5 = By.xpath("/html/body/div[3]/div[2]/div/div/div/button[2]");
	// ======================================= Action Delete Confirmation  ===========================================================
	By delConfirm1 = By.xpath("/html/body/div[3]/div[2]/div/mat-dialog-container/app-confirmation-boxes/div/div[2]/button[2]");
	By delConfirm2 = By.xpath("/html/body/div[3]/div[2]/div/mat-dialog-container/app-confirmation-boxes/div/div[2]/button[2]");
	By delConfirm3 = By.xpath("/html/body/div[3]/div[2]/div/mat-dialog-container/app-confirmation-boxes/div/div[2]/button[2]");
	By delConfirm4 = By.xpath("/html/body/div[3]/div[2]/div/mat-dialog-container/app-confirmation-boxes/div/div[2]/button[2]");
	By delConfirm5 = By.xpath("/html/body/div[3]/div[2]/div/mat-dialog-container/app-confirmation-boxes/div/div[2]/button[2]");
	// ====================================== Action Delete >> Cancel  ================================================================
	By delCancel1 = By.xpath("/html/body/div[3]/div[2]/div/mat-dialog-container/app-confirmation-boxes/div/div[2]/button[1]");
	By delCancel2 = By.xpath("/html/body/div[3]/div[2]/div/mat-dialog-container/app-confirmation-boxes/div/div[2]/button[1]");
	By delCancel3 = By.xpath("/html/body/div[3]/div[2]/div/mat-dialog-container/app-confirmation-boxes/div/div[2]/button[1]");
	By delCancel4 = By.xpath("/html/body/div[3]/div[2]/div/mat-dialog-container/app-confirmation-boxes/div/div[2]/button[1]");
	By delCancel5 = By.xpath("/html/body/div[3]/div[2]/div/mat-dialog-container/app-confirmation-boxes/div/div[2]/button[1]");

	//	By editBtn2 = By.xpath("//*[@id=\"cdk-overlay-1\"]/div/div/button[1]/mat-icon");
	By deleteBtn1 = By.xpath("/html/body/div[3]/div[2]/div/div/div/button[2]/span");

	By editSuccess = By.xpath("//*[text()='Success']");
	By editFailure = By.xpath("//*[text()='Failure']");
	By delsuccess = By.xpath("//*[text()='Success']");

	By cancelIcon1 = By.xpath("//app-chargerconfiguration/mat-card[2]/form/div/table/tbody/tr[1]/td[3]/div/i");
	By cancelIcon2 = By.xpath("/html/body/app-root/app-super-admin/mat-sidenav-container/mat-sidenav-content/app-chargerconfiguration/mat-card[2]/form/div/table/tbody/tr[2]/td[3]/div/i");
	By cancelIcon3 = By.xpath("//app-chargerconfiguration/mat-card[2]/form/div/table/tbody/tr[3]/td[3]/div/i");
	By cancelIcon4 = By.xpath("//app-chargerconfiguration/mat-card[2]/form/div/table/tbody/tr[4]/td[3]/div/i");
	By cancelIcon5 = By.xpath("//app-chargerconfiguration/mat-card[2]/form/div/table/tbody/tr[5]/td[3]/div/i");
	By saveBtn1 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-chargerconfiguration/mat-card[2]/form/div/table/tbody/tr[1]/td[3]/div/button");
	By saveBtn2 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-chargerconfiguration/mat-card[2]/form/div/table/tbody/tr[2]/td[3]/div/button");
	By saveBtn3 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-chargerconfiguration/mat-card[2]/form/div/table/tbody/tr[3]/td[3]/div/button");
	By saveBtn4 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-chargerconfiguration/mat-card[2]/form/div/table/tbody/tr[4]/td[3]/div/button");
	By saveBtn5 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-chargerconfiguration/mat-card[2]/form/div/table/tbody/tr[5]/td[3]/div/button");
	By confCanclBtn = By.xpath("/html/body/div[3]/div[2]/div/mat-dialog-container/app-confirmation-boxes/div/div[2]/button[1]");
	By confDelBtn = By.xpath("/html/body/div[3]/div[2]/div/mat-dialog-container/app-confirmation-boxes/div/div[2]/button[2]");
	By editInput = By.xpath("//app-chargerconfiguration/mat-card[2]/form/div/table/tbody/tr[5]/td[2]/input");
	By editInput1 = By.xpath("/html/body/app-root/app-super-admin/mat-sidenav-container/mat-sidenav-content/app-chargerconfiguration/mat-card[2]/form/div/table/tbody/tr[1]/td[2]/input");
	By editInput2 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-chargerconfiguration/mat-card[2]/form/div/table/tbody/tr[2]/td[2]/input");
	By editInput3 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-chargerconfiguration/mat-card[2]/form/div/table/tbody/tr[3]/td[2]/input");
	By editInput4 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-chargerconfiguration/mat-card[2]/form/div/table/tbody/tr[4]/td[2]/input");
	By editInput5 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-chargerconfiguration/mat-card[2]/form/div/table/tbody/tr[5]/td[2]/input");
	By confDelBtn2 = By.xpath("//app-confirmation-boxes/div/div[2]/button[2]");
	By additionELem = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-chargerconfiguration/mat-card[2]/div[1]");

	// Connector Configuration	
	By MConfig1 = By.xpath("//*[text()=' Master Configuration ']");
	By ConCnfig = By.xpath("//*[text()=' Connector Configuration ']");
	By addConnect = By.xpath("//app-connectorconfiguration/mat-card[1]/form/div/div/div[1]/input");
	By ChargeCap = By.xpath("//app-connectorconfiguration/mat-card[1]/form/div/div/div[2]/input");
	By CapUnitDdn = By.xpath("//app-connectorconfiguration/mat-card[1]/form/div/div/div[3]/select");
	By CapUnit = By.xpath("//app-connectorconfiguration/mat-card[1]/form/div/div/div[3]/select/option[2]");
	By SbmtBtn2 = By.xpath("//app-connectorconfiguration/mat-card[1]/div[2]/button[2]");
	By nxtPage = By.xpath("//pagination-controls/pagination-template/ul/li[4]/span[2]");
	By action1 = By.xpath("//app-connectorconfiguration/mat-card[2]/form/div/table/tbody/tr[3]/td[4]/app-moredetaibtn/button/span/mat-icon");

	public static LoginPage lps = new LoginPage();

	public void add_Edit_Delete_Connector() throws InterruptedException, IOException {
		logger.log(Status.INFO, "The user is logged in with Super Admin credentials");
		lps.login2();
		WebDriverWait wait = new WebDriverWait(driver, 30);
		drv.waitForAngularRequestsToFinish();
		wait.until(ExpectedConditions.elementToBeClickable(MConfig)).click();
		wait.until(ExpectedConditions.elementToBeClickable(connetConfig)).click();
		rows = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(connecttable));
		//	=======================================================     Addition of New Connector   =============================================================
		if(rows.isEmpty()) {
			wait.until(ExpectedConditions.presenceOfElementLocated(addConctType)).clear();
			wait.until(ExpectedConditions.presenceOfElementLocated(addConctType)).sendKeys(ExcelReader.connector1());
			wait.until(ExpectedConditions.presenceOfElementLocated(addChargCapcty)).clear();
			wait.until(ExpectedConditions.presenceOfElementLocated(addChargCapcty)).sendKeys(ExcelReader.connectorCap1());
			wait.until(ExpectedConditions.presenceOfElementLocated(chrgCapctyUnit)).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(selectCapctyUnit)).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(sbmtBtn1)).click();
			drv.waitForAngularRequestsToFinish();
			Thread.sleep(1000);
			driver.navigate().to("https://i.dev.conformity.co/super-admin/masterconnector");
		}
		else {
			logger.log(Status.INFO, "Data is already available");
		}
		System.out.println(rows.size());
		rowSize= rows.size();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//mat-sidenav-container/mat-sidenav-content/app-connectorconfiguration/mat-card[2]/div[1]/span"))).click();
		act = new Actions(driver);
		act.sendKeys(Keys.PAGE_DOWN).build().perform();
		// ========================================================================================================		
		if(rowSize==1 && rowSize<2 && wait.until(ExpectedConditions.presenceOfElementLocated(editInputB1)).getAttribute("value").contentEquals(connectName)) {
			logger.log(Status.INFO, "The Connector Type is already available");
		}else if(rowSize==2 && rowSize<3 && wait.until(ExpectedConditions.presenceOfElementLocated(editInputB1)).getAttribute("value").contentEquals(connectName) ^ wait.until(ExpectedConditions.presenceOfElementLocated(editInputB2)).getAttribute("value").contentEquals(connectName)) {
			logger.log(Status.INFO, "The Connector Type is already available");
		}else if(rowSize==3 && rowSize<4 &&wait.until(ExpectedConditions.presenceOfElementLocated(editInputB1)).getAttribute("value").contentEquals(connectName) ^ wait.until(ExpectedConditions.presenceOfElementLocated(editInputB2)).getAttribute("value").contentEquals(connectName) ^ wait.until(ExpectedConditions.presenceOfElementLocated(editInputB3)).getAttribute("value").contentEquals(connectName)) {
			logger.log(Status.INFO, "The Connector Type is already available");
		}else if(rowSize==4 && rowSize<5 && wait.until(ExpectedConditions.presenceOfElementLocated(editInputB1)).getAttribute("value").contentEquals(connectName) ^ wait.until(ExpectedConditions.presenceOfElementLocated(editInputB2)).getAttribute("value").contentEquals(connectName) ^ wait.until(ExpectedConditions.presenceOfElementLocated(editInputB3)).getAttribute("value").contentEquals(connectName) ^ wait.until(ExpectedConditions.presenceOfElementLocated(editInputB4)).getAttribute("value").contentEquals(connectName)) {
			logger.log(Status.INFO, "The Connector Type is already available");
		}else if(rowSize==5 && rowSize<6 && wait.until(ExpectedConditions.presenceOfElementLocated(editInputB1)).getAttribute("value").contentEquals(connectName) ^ wait.until(ExpectedConditions.presenceOfElementLocated(editInputB2)).getAttribute("value").contentEquals(connectName) ^ wait.until(ExpectedConditions.presenceOfElementLocated(editInputB3)).getAttribute("value").contentEquals(connectName) ^ wait.until(ExpectedConditions.presenceOfElementLocated(editInputB4)).getAttribute("value").contentEquals(connectName) ^ wait.until(ExpectedConditions.presenceOfElementLocated(editInputB5)).getAttribute("value").contentEquals(connectName)) {
			logger.log(Status.INFO, "The Connector Type is already available");
		}
		else {
			wait.until(ExpectedConditions.presenceOfElementLocated(addConctType)).clear();
			wait.until(ExpectedConditions.presenceOfElementLocated(addConctType)).sendKeys(ExcelReader.connector1());
			wait.until(ExpectedConditions.presenceOfElementLocated(addChargCapcty)).clear();
			wait.until(ExpectedConditions.presenceOfElementLocated(addChargCapcty)).sendKeys(ExcelReader.connectorCap1());
			wait.until(ExpectedConditions.presenceOfElementLocated(chrgCapctyUnit)).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(selectCapctyUnit)).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(sbmtBtn1)).click();
			drv.waitForAngularRequestsToFinish();
			Thread.sleep(1000);
			driver.navigate().to("https://i.dev.conformity.co/super-admin/masterconnector");
			logger.log(Status.INFO, "Connector Type Added Successfully");
		}

		Thread.sleep(1000);
		lps.login4();
		// =======================================================================================================================================================		
		//=======================================================     Modification of the Connector Name   =============================================================
		lps.login2();
		drv.waitForAngularRequestsToFinish();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.elementToBeClickable(MConfig)).click();
		wait.until(ExpectedConditions.elementToBeClickable(connetConfig)).click();
		rows = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(connecttable));
		if(rows.isEmpty()) {
			wait.until(ExpectedConditions.presenceOfElementLocated(addConctType)).clear();
			wait.until(ExpectedConditions.presenceOfElementLocated(addConctType)).sendKeys(ExcelReader.connector1());
			wait.until(ExpectedConditions.presenceOfElementLocated(addChargCapcty)).clear();
			wait.until(ExpectedConditions.presenceOfElementLocated(addChargCapcty)).sendKeys(ExcelReader.connectorCap1());
			wait.until(ExpectedConditions.presenceOfElementLocated(chrgCapctyUnit)).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(selectCapctyUnit)).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(sbmtBtn1)).click();
			drv.waitForAngularRequestsToFinish();
			Thread.sleep(1000);
			driver.navigate().to("https://i.dev.conformity.co/super-admin/masterconnector");
		}
		else {
			System.out.println(rows.size());
			rowSize= rows.size();
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//mat-sidenav-container/mat-sidenav-content/app-connectorconfiguration/mat-card[2]/div[1]/span"))).click();
			act = new Actions(driver);
			act.sendKeys(Keys.PAGE_DOWN).build().perform();

			if(rowSize==1 && rowSize<2 && wait.until(ExpectedConditions.presenceOfElementLocated(edtInput1)).getAttribute("value").contentEquals(connectName)) {
				logger.log(Status.INFO, "Line 272 - The Connector Type is available in row 1 hence processing modification");
				wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon1)).click();
				wait.until(ExpectedConditions.presenceOfElementLocated(editBtn1)).click();
				wait.until(ExpectedConditions.presenceOfElementLocated(edtInput1)).clear();
				wait.until(ExpectedConditions.presenceOfElementLocated(edtInput1)).sendKeys(ExcelReader.connector2());
				wait.until(ExpectedConditions.presenceOfElementLocated(edInput1)).clear();
				wait.until(ExpectedConditions.presenceOfElementLocated(edInput1)).sendKeys(ExcelReader.connectorCap2());
				wait.until(ExpectedConditions.presenceOfElementLocated(sveBtn1)).click();
				logger.log(Status.INFO, "Line 280 - Modification Completed");
				drv.waitForAngularRequestsToFinish();
				Thread.sleep(3000);
			}
			else if(rowSize==2 && rowSize<3) {
				if(wait.until(ExpectedConditions.presenceOfElementLocated(edtInput1)).getAttribute("value").contentEquals(connectName)) {
					logger.log(Status.INFO, "Line 286 - The Connector Type is available in row 1 hence processing modification");
					wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon1)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(editBtn1)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(edtInput1)).clear();
					wait.until(ExpectedConditions.presenceOfElementLocated(edtInput1)).sendKeys(ExcelReader.connector2());
					wait.until(ExpectedConditions.presenceOfElementLocated(edInput1)).clear();
					wait.until(ExpectedConditions.presenceOfElementLocated(edInput1)).sendKeys(ExcelReader.connectorCap2());
					wait.until(ExpectedConditions.presenceOfElementLocated(sveBtn1)).click();
					logger.log(Status.INFO, "Line 294 - Modification Completed");
					drv.waitForAngularRequestsToFinish();
					Thread.sleep(3000);
				}else if(wait.until(ExpectedConditions.presenceOfElementLocated(edtInput2)).getAttribute("value").contentEquals(connectName)) {
					logger.log(Status.INFO, "Line 286 - The Connector Type is available in row 2 hence processing modification");
					wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon2)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(editBtn1)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(edtInput2)).clear();
					wait.until(ExpectedConditions.presenceOfElementLocated(edtInput2)).sendKeys(ExcelReader.connector2());
					wait.until(ExpectedConditions.presenceOfElementLocated(edInput2)).clear();
					wait.until(ExpectedConditions.presenceOfElementLocated(edInput2)).sendKeys(ExcelReader.connectorCap2());
					wait.until(ExpectedConditions.presenceOfElementLocated(sveBtn2)).click();
					logger.log(Status.INFO, "Line 306 - Modification Completed");
					drv.waitForAngularRequestsToFinish();
					Thread.sleep(3000);
				}

			}
			else if(rowSize==3 && rowSize<4) {
				if(wait.until(ExpectedConditions.presenceOfElementLocated(edtInput1)).getAttribute("value").contentEquals(connectName)) {
					logger.log(Status.INFO, "Line 314 - The Connector Type is available in row 1 hence processing modification");
					wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon1)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(editBtn1)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(edtInput1)).clear();
					wait.until(ExpectedConditions.presenceOfElementLocated(edtInput1)).sendKeys(ExcelReader.connector2());
					wait.until(ExpectedConditions.presenceOfElementLocated(edInput1)).clear();
					wait.until(ExpectedConditions.presenceOfElementLocated(edInput1)).sendKeys(ExcelReader.connectorCap2());
					wait.until(ExpectedConditions.presenceOfElementLocated(sveBtn1)).click();
					logger.log(Status.INFO, "Line 322 - Modification Completed");
					drv.waitForAngularRequestsToFinish();
					Thread.sleep(3000);
				}else if(wait.until(ExpectedConditions.presenceOfElementLocated(edtInput2)).getAttribute("value").contentEquals(connectName)) {
					logger.log(Status.INFO, "Line 326 - The Connector Type is available in row 2 hence processing modification");
					wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon2)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(editBtn1)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(edtInput2)).clear();
					wait.until(ExpectedConditions.presenceOfElementLocated(edtInput2)).sendKeys(ExcelReader.connector2());
					wait.until(ExpectedConditions.presenceOfElementLocated(edInput2)).clear();
					wait.until(ExpectedConditions.presenceOfElementLocated(edInput2)).sendKeys(ExcelReader.connectorCap2());
					wait.until(ExpectedConditions.presenceOfElementLocated(sveBtn2)).click();
					logger.log(Status.INFO, "Line 334 - Modification Completed");
					drv.waitForAngularRequestsToFinish();
					Thread.sleep(3000);
				}else if(wait.until(ExpectedConditions.presenceOfElementLocated(edtInput3)).getAttribute("value").contentEquals(connectName)) {
					logger.log(Status.INFO, "Line 338 - The Connector Type is available in row 3 hence processing modification");
					wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon3)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(editBtn3)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(edtInput3)).clear();
					wait.until(ExpectedConditions.presenceOfElementLocated(edtInput3)).sendKeys(ExcelReader.connector2());
					wait.until(ExpectedConditions.presenceOfElementLocated(edInput3)).clear();
					wait.until(ExpectedConditions.presenceOfElementLocated(edInput3)).sendKeys(ExcelReader.connectorCap2());
					wait.until(ExpectedConditions.presenceOfElementLocated(sveBtn3)).click();
					logger.log(Status.INFO, "Line 346 - Modification Completed");
					drv.waitForAngularRequestsToFinish();
					Thread.sleep(3000);
				}

			}else if(rowSize==4 && rowSize<5) {
				if(wait.until(ExpectedConditions.presenceOfElementLocated(edtInput1)).getAttribute("value").contentEquals(connectName)) {
					logger.log(Status.INFO, "Line 353 - The Connector Type is available in row 1 hence processing modification");
					wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon1)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(editBtn1)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(edtInput1)).clear();
					wait.until(ExpectedConditions.presenceOfElementLocated(edtInput1)).sendKeys(ExcelReader.connector2());
					wait.until(ExpectedConditions.presenceOfElementLocated(edInput1)).clear();
					wait.until(ExpectedConditions.presenceOfElementLocated(edInput1)).sendKeys(ExcelReader.connectorCap2());
					wait.until(ExpectedConditions.presenceOfElementLocated(sveBtn1)).click();
					logger.log(Status.INFO, "Line 361 - Modification Completed");
					drv.waitForAngularRequestsToFinish();
					Thread.sleep(3000);
				}else if(wait.until(ExpectedConditions.presenceOfElementLocated(edtInput2)).getAttribute("value").contentEquals(connectName)) {
					logger.log(Status.INFO, "Line 365 - The Connector Type is available in row 2 hence processing modification");
					wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon2)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(editBtn2)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(edtInput2)).clear();
					wait.until(ExpectedConditions.presenceOfElementLocated(edtInput2)).sendKeys(ExcelReader.connector2());
					wait.until(ExpectedConditions.presenceOfElementLocated(edInput2)).clear();
					wait.until(ExpectedConditions.presenceOfElementLocated(edInput2)).sendKeys(ExcelReader.connectorCap2());
					wait.until(ExpectedConditions.presenceOfElementLocated(sveBtn2)).click();
					logger.log(Status.INFO, "Line 373 - Modification Completed");
					drv.waitForAngularRequestsToFinish();
					Thread.sleep(3000);
				}else if(wait.until(ExpectedConditions.presenceOfElementLocated(edtInput3)).getAttribute("value").contentEquals(connectName)) {
					logger.log(Status.INFO, "Line 377 - The Connector Type is available in row 3 hence processing modification");
					wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon3)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(editBtn3)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(edtInput3)).clear();
					wait.until(ExpectedConditions.presenceOfElementLocated(edtInput3)).sendKeys(ExcelReader.connector2());
					wait.until(ExpectedConditions.presenceOfElementLocated(edInput3)).clear();
					wait.until(ExpectedConditions.presenceOfElementLocated(edInput3)).sendKeys(ExcelReader.connectorCap2());
					wait.until(ExpectedConditions.presenceOfElementLocated(sveBtn3)).click();
					logger.log(Status.INFO, "Line 385 - Modification Completed");
					drv.waitForAngularRequestsToFinish();
					Thread.sleep(3000);
				}else if(wait.until(ExpectedConditions.presenceOfElementLocated(edtInput4)).getAttribute("value").contentEquals(connectName)) {
					logger.log(Status.INFO, "Line 389 - The Connector Type is available in row 4 hence processing modification");
					wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon4)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(editBtn4)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(edtInput4)).clear();
					wait.until(ExpectedConditions.presenceOfElementLocated(edtInput4)).sendKeys(ExcelReader.connector2());
					wait.until(ExpectedConditions.presenceOfElementLocated(edInput4)).clear();
					wait.until(ExpectedConditions.presenceOfElementLocated(edInput4)).sendKeys(ExcelReader.connectorCap2());
					wait.until(ExpectedConditions.presenceOfElementLocated(sveBtn4)).click();
					logger.log(Status.INFO, "Line 397 - Modification Completed");
					drv.waitForAngularRequestsToFinish();
					Thread.sleep(3000);
				}
			}
			else if(rowSize==5 && rowSize<6) {
				if(wait.until(ExpectedConditions.presenceOfElementLocated(edtInput1)).getAttribute("value").contentEquals(connectName)) {
					logger.log(Status.INFO, "Line 404 - The Connector Type is available in row 1 hence processing modification");
					wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon1)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(editBtn1)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(edtInput1)).clear();
					wait.until(ExpectedConditions.presenceOfElementLocated(edtInput1)).sendKeys(ExcelReader.connector2());
					wait.until(ExpectedConditions.presenceOfElementLocated(edInput1)).clear();
					wait.until(ExpectedConditions.presenceOfElementLocated(edInput1)).sendKeys(ExcelReader.connectorCap2());
					wait.until(ExpectedConditions.presenceOfElementLocated(sveBtn1)).click();
					logger.log(Status.INFO, "Line 412 - Modification Completed");
					drv.waitForAngularRequestsToFinish();
					Thread.sleep(3000);
				}else if(wait.until(ExpectedConditions.presenceOfElementLocated(edtInput2)).getAttribute("value").contentEquals(connectName)) {
					logger.log(Status.INFO, "Line 416 - The Connector Type is available in row 2 hence processing modification");
					wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon2)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(editBtn2)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(edtInput2)).clear();
					wait.until(ExpectedConditions.presenceOfElementLocated(edtInput2)).sendKeys(ExcelReader.connector2());
					wait.until(ExpectedConditions.presenceOfElementLocated(edInput2)).clear();
					wait.until(ExpectedConditions.presenceOfElementLocated(edInput2)).sendKeys(ExcelReader.connectorCap2());
					wait.until(ExpectedConditions.presenceOfElementLocated(sveBtn2)).click();
					logger.log(Status.INFO, "Line 424 - Modification Completed");
					drv.waitForAngularRequestsToFinish();
					Thread.sleep(3000);
				}else if(wait.until(ExpectedConditions.presenceOfElementLocated(edtInput3)).getAttribute("value").contentEquals(connectName)) {
					logger.log(Status.INFO, "Line 428 - The Connector Type is available in row 3 hence processing modification");
					wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon3)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(editBtn3)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(edtInput3)).clear();
					wait.until(ExpectedConditions.presenceOfElementLocated(edtInput3)).sendKeys(ExcelReader.connector2());
					wait.until(ExpectedConditions.presenceOfElementLocated(edInput3)).clear();
					wait.until(ExpectedConditions.presenceOfElementLocated(edInput3)).sendKeys(ExcelReader.connectorCap2());
					wait.until(ExpectedConditions.presenceOfElementLocated(sveBtn3)).click();
					logger.log(Status.INFO, "Line 436 - Modification Completed");
					drv.waitForAngularRequestsToFinish();
					Thread.sleep(3000);
				}else if(wait.until(ExpectedConditions.presenceOfElementLocated(edtInput4)).getAttribute("value").contentEquals(connectName)) {
					logger.log(Status.INFO, "Line 440 - The Connector Type is available in row 4 hence processing modification");
					wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon4)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(editBtn4)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(edtInput4)).clear();
					wait.until(ExpectedConditions.presenceOfElementLocated(edtInput4)).sendKeys(ExcelReader.connector2());
					wait.until(ExpectedConditions.presenceOfElementLocated(edInput4)).clear();
					wait.until(ExpectedConditions.presenceOfElementLocated(edInput4)).sendKeys(ExcelReader.connectorCap2());
					wait.until(ExpectedConditions.presenceOfElementLocated(sveBtn4)).click();
					logger.log(Status.INFO, "Line 448 - Modification Completed");
					drv.waitForAngularRequestsToFinish();
					Thread.sleep(3000);
				}else if(wait.until(ExpectedConditions.presenceOfElementLocated(edtInput5)).getAttribute("value").contentEquals(connectName)) {
					logger.log(Status.INFO, "Line 452 - The Connector Type is available in row 4 hence processing modification");
					wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon5)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(editBtn5)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(edtInput5)).clear();
					wait.until(ExpectedConditions.presenceOfElementLocated(edtInput5)).sendKeys(ExcelReader.connector2());
					wait.until(ExpectedConditions.presenceOfElementLocated(edInput5)).clear();
					wait.until(ExpectedConditions.presenceOfElementLocated(edInput5)).sendKeys(ExcelReader.connectorCap2());
					wait.until(ExpectedConditions.presenceOfElementLocated(sveBtn5)).click();
					logger.log(Status.INFO, "Line 460 - Modification Completed");
					drv.waitForAngularRequestsToFinish();
					Thread.sleep(3000);
				}
			}
			else {
				logger.log(Status.INFO, "Line472 - The Connector Type is not matched");
			}
		}
		Thread.sleep(6000);
		driver.get("https://i.dev.conformity.co/super-admin/masterconnector");
		drv.waitForAngularRequestsToFinish();
		//  =================================================  Deletion of matched record  ===========================================================		
		Thread.sleep(3000);
		
		wait.until(ExpectedConditions.elementToBeClickable(MConfig)).click();
		wait.until(ExpectedConditions.elementToBeClickable(connetConfig)).click();
		rows = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(connecttable));
		if(rows.isEmpty()) {
			wait.until(ExpectedConditions.presenceOfElementLocated(addConctType)).clear();
			wait.until(ExpectedConditions.presenceOfElementLocated(addConctType)).sendKeys(ExcelReader.connector2());
			wait.until(ExpectedConditions.presenceOfElementLocated(addChargCapcty)).clear();
			wait.until(ExpectedConditions.presenceOfElementLocated(addChargCapcty)).sendKeys(ExcelReader.connectorCap2());
			wait.until(ExpectedConditions.presenceOfElementLocated(chrgCapctyUnit)).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(selectCapctyUnit)).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(sbmtBtn1)).click();
			drv.waitForAngularRequestsToFinish();
			Thread.sleep(1000);
			driver.navigate().to("https://i.dev.conformity.co/super-admin/masterconnector");
		}
		else {
			System.out.println(rows.size());
			rowSize= rows.size();
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//mat-sidenav-container/mat-sidenav-content/app-connectorconfiguration/mat-card[2]/div[1]/span"))).click();
			act = new Actions(driver);
			act.sendKeys(Keys.PAGE_DOWN).build().perform();

			if(rowSize==1 && rowSize<2 && wait.until(ExpectedConditions.presenceOfElementLocated(edtInput1)).getAttribute("value").contentEquals(delConnect)) {
				logger.log(Status.INFO, "Line283 - The Connector Type is already available in row 1");
				wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon1)).click();
				wait.until(ExpectedConditions.presenceOfElementLocated(delbtn1)).click();
				wait.until(ExpectedConditions.presenceOfElementLocated(delCancel1)).click();
				wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon1)).click();
				wait.until(ExpectedConditions.presenceOfElementLocated(delbtn1)).click();
				wait.until(ExpectedConditions.presenceOfElementLocated(delConfirm1)).click();
				drv.waitForAngularRequestsToFinish();
				Thread.sleep(3000);
			}
			else if(rowSize==2 && rowSize<3) {
				if(wait.until(ExpectedConditions.presenceOfElementLocated(edtInput1)).getAttribute("value").contentEquals(delConnect)) {
					logger.log(Status.INFO, "Line396 - The Connector Type is already available in row 1");
					wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon1)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delbtn1)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delCancel1)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon1)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delbtn1)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delConfirm1)).click();
					drv.waitForAngularRequestsToFinish();
					Thread.sleep(3000);
				}else if(wait.until(ExpectedConditions.presenceOfElementLocated(edtInput2)).getAttribute("value").contentEquals(delConnect)) {
					logger.log(Status.INFO, "Line307 - The Connector Type is already available in row 2");
					wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon2)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delbtn1)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delCancel2)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon2)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delbtn1)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delConfirm2)).click();
					drv.waitForAngularRequestsToFinish();
					Thread.sleep(3000);
				}
				else {
					System.out.println("The row size is exceeded or less");
				}
			}
			else if(rowSize==3 && rowSize<4) {
				if(wait.until(ExpectedConditions.presenceOfElementLocated(edtInput1)).getAttribute("value").contentEquals(delConnect)) {
					logger.log(Status.INFO, "Line324 - The Connector Type is already available in row 1");
					wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon1)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delbtn1)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delCancel1)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon1)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delbtn1)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delConfirm1)).click();
					drv.waitForAngularRequestsToFinish();
					Thread.sleep(3000);
				}else if(wait.until(ExpectedConditions.presenceOfElementLocated(edtInput2)).getAttribute("value").contentEquals(delConnect)) {
					logger.log(Status.INFO, "Line335 - The Connector Type is already available in row 2");
					wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon2)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delbtn1)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delCancel2)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon2)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delbtn1)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delConfirm2)).click();
					drv.waitForAngularRequestsToFinish();
					Thread.sleep(3000);
				}else if(wait.until(ExpectedConditions.presenceOfElementLocated(edtInput3)).getAttribute("value").contentEquals(delConnect)) {
					logger.log(Status.INFO, "Line346 - The Connector Type is already available in row 3");
					wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon3)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delbtn1)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delCancel3)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon3)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delbtn1)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delConfirm3)).click();
					drv.waitForAngularRequestsToFinish();
					Thread.sleep(3000);
				}
				else {
					System.out.println("The row size is exceeded or less");
				}
			}else if(rowSize==4 && rowSize<5) {
				if(wait.until(ExpectedConditions.presenceOfElementLocated(edtInput1)).getAttribute("value").contentEquals(delConnect)) {
					logger.log(Status.INFO, "Line362 - The Connector Type is already available in row 1");
					wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon1)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delbtn1)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delCancel1)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon1)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delbtn1)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delConfirm1)).click();
					drv.waitForAngularRequestsToFinish();
					Thread.sleep(3000);
				}else if(wait.until(ExpectedConditions.presenceOfElementLocated(edtInput2)).getAttribute("value").contentEquals(delConnect)) {
					logger.log(Status.INFO, "Line373 - The Connector Type is already available in row 2");
					wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon2)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delbtn1)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delCancel2)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon2)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delbtn1)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delConfirm2)).click();
					drv.waitForAngularRequestsToFinish();
					Thread.sleep(3000);
				}else if(wait.until(ExpectedConditions.presenceOfElementLocated(edtInput3)).getAttribute("value").contentEquals(delConnect)) {
					logger.log(Status.INFO, "Line384 - The Connector Type is already available in row 3");
					wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon3)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delbtn1)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delCancel3)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon3)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delbtn1)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delConfirm3)).click();
					drv.waitForAngularRequestsToFinish();
					Thread.sleep(3000);
				}else if(wait.until(ExpectedConditions.presenceOfElementLocated(edtInput4)).getAttribute("value").contentEquals(delConnect)) {
					logger.log(Status.INFO, "Line600 - The Connector Type is already available in row 4");
					wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon4)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delbtn1)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delCancel4)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon4)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delbtn1)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delConfirm4)).click();
					drv.waitForAngularRequestsToFinish();
					Thread.sleep(3000);
				}
				else {
					System.out.println("The row size is exceeded or less");
				}
			}
			else if(rowSize==5 && rowSize<6) {
				if(wait.until(ExpectedConditions.presenceOfElementLocated(edtInput1)).getAttribute("value").contentEquals(delConnect)) {
					logger.log(Status.INFO, "Line412 - The Connector Type is already available in row 1");
					wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon1)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delbtn1)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delCancel1)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon1)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delbtn1)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delConfirm1)).click();
					drv.waitForAngularRequestsToFinish();
					Thread.sleep(3000);
				}else if(wait.until(ExpectedConditions.presenceOfElementLocated(edtInput2)).getAttribute("value").contentEquals(delConnect)) {
					logger.log(Status.INFO, "Line423 - The Connector Type is already available in row 2");
					wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon2)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delbtn1)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delCancel2)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon2)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delbtn1)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delConfirm2)).click();
					drv.waitForAngularRequestsToFinish();
					Thread.sleep(3000);
				}else if(wait.until(ExpectedConditions.presenceOfElementLocated(edtInput3)).getAttribute("value").contentEquals(delConnect)) {
					logger.log(Status.INFO, "Line434 - The Connector Type is already available in row 3");
					wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon3)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delbtn1)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delCancel3)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon3)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delbtn1)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delConfirm3)).click();
					drv.waitForAngularRequestsToFinish();
					Thread.sleep(3000);
				}else if(wait.until(ExpectedConditions.presenceOfElementLocated(edtInput4)).getAttribute("value").contentEquals(delConnect)) {
					logger.log(Status.INFO, "Line445 - The Connector Type is already available in row 4");
					wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon4)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delbtn1)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delCancel4)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon4)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delbtn1)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delConfirm4)).click();
					drv.waitForAngularRequestsToFinish();
					Thread.sleep(3000);
				}else if(wait.until(ExpectedConditions.presenceOfElementLocated(edtInput5)).getAttribute("value").contentEquals(delConnect)) {
					logger.log(Status.INFO, "Line456 - The Connector Type is already available in row 5");
					wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon5)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delbtn1)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delCancel5)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon5)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delbtn1)).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(delConfirm5)).click();
					drv.waitForAngularRequestsToFinish();
					Thread.sleep(3000);
				}
				else {
					System.out.println("The row size is exceeded or less");
				}
			}
			else {
				logger.log(Status.INFO, "Line695 - The Connector Type is not matched");

			}
		}
		Thread.sleep(6000);
		lps.login4();
	}

	public void add1_Edit_Delete_Connector() throws InterruptedException, IOException {
		logger.log(Status.INFO, "The user is logged in with Super Admin credentials");
		lps.login2();
		WebDriverWait wait = new WebDriverWait(driver, 30);
		drv.waitForAngularRequestsToFinish();
		wait.until(ExpectedConditions.elementToBeClickable(MConfig)).click();
		wait.until(ExpectedConditions.elementToBeClickable(connetConfig)).click();
		rows = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(connecttable));
		//	=======================================================     Addition of New Connector   =============================================================
		if(rows.isEmpty()) {
			wait.until(ExpectedConditions.presenceOfElementLocated(addConctType)).clear();
			wait.until(ExpectedConditions.presenceOfElementLocated(addConctType)).sendKeys(ExcelReader.connector1());
			wait.until(ExpectedConditions.presenceOfElementLocated(addChargCapcty)).clear();
			wait.until(ExpectedConditions.presenceOfElementLocated(addChargCapcty)).sendKeys(ExcelReader.connectorCap1());
			wait.until(ExpectedConditions.presenceOfElementLocated(chrgCapctyUnit)).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(selectCapctyUnit)).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(sbmtBtn1)).click();
			drv.waitForAngularRequestsToFinish();
			Thread.sleep(1000);
			driver.navigate().to("https://i.dev.conformity.co/super-admin/masterconnector");
		}
		else {
			logger.log(Status.INFO, "Data is already available");
		}
		System.out.println(rows.size());
		rowSize= rows.size();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//mat-sidenav-container/mat-sidenav-content/app-connectorconfiguration/mat-card[2]/div[1]/span"))).click();
		act = new Actions(driver);
		act.sendKeys(Keys.PAGE_DOWN).build().perform();
		// ========================================================================================================		
		if(rowSize==1 && rowSize<2 && wait.until(ExpectedConditions.presenceOfElementLocated(editInputB1)).getAttribute("value").contentEquals(connectName)) {
			logger.log(Status.INFO, "The Connector Type is already available");
		}else if(rowSize==2 && rowSize<3 && wait.until(ExpectedConditions.presenceOfElementLocated(editInputB1)).getAttribute("value").contentEquals(connectName) ^ wait.until(ExpectedConditions.presenceOfElementLocated(editInputB2)).getAttribute("value").contentEquals(connectName)) {
			logger.log(Status.INFO, "The Connector Type is already available");
		}else if(rowSize==3 && rowSize<4 &&wait.until(ExpectedConditions.presenceOfElementLocated(editInputB1)).getAttribute("value").contentEquals(connectName) ^ wait.until(ExpectedConditions.presenceOfElementLocated(editInputB2)).getAttribute("value").contentEquals(connectName) ^ wait.until(ExpectedConditions.presenceOfElementLocated(editInputB3)).getAttribute("value").contentEquals(connectName)) {
			logger.log(Status.INFO, "The Connector Type is already available");
		}else if(rowSize==4 && rowSize<5 && wait.until(ExpectedConditions.presenceOfElementLocated(editInputB1)).getAttribute("value").contentEquals(connectName) ^ wait.until(ExpectedConditions.presenceOfElementLocated(editInputB2)).getAttribute("value").contentEquals(connectName) ^ wait.until(ExpectedConditions.presenceOfElementLocated(editInputB3)).getAttribute("value").contentEquals(connectName) ^ wait.until(ExpectedConditions.presenceOfElementLocated(editInputB4)).getAttribute("value").contentEquals(connectName)) {
			logger.log(Status.INFO, "The Connector Type is already available");
		}else if(rowSize==5 && rowSize<6 && wait.until(ExpectedConditions.presenceOfElementLocated(editInputB1)).getAttribute("value").contentEquals(connectName) ^ wait.until(ExpectedConditions.presenceOfElementLocated(editInputB2)).getAttribute("value").contentEquals(connectName) ^ wait.until(ExpectedConditions.presenceOfElementLocated(editInputB3)).getAttribute("value").contentEquals(connectName) ^ wait.until(ExpectedConditions.presenceOfElementLocated(editInputB4)).getAttribute("value").contentEquals(connectName) ^ wait.until(ExpectedConditions.presenceOfElementLocated(editInputB5)).getAttribute("value").contentEquals(connectName)) {
			logger.log(Status.INFO, "The Connector Type is already available");
		}
		else {
			wait.until(ExpectedConditions.presenceOfElementLocated(addConctType)).clear();
			wait.until(ExpectedConditions.presenceOfElementLocated(addConctType)).sendKeys(ExcelReader.connector1());
			wait.until(ExpectedConditions.presenceOfElementLocated(addChargCapcty)).clear();
			wait.until(ExpectedConditions.presenceOfElementLocated(addChargCapcty)).sendKeys(ExcelReader.connectorCap1());
			wait.until(ExpectedConditions.presenceOfElementLocated(chrgCapctyUnit)).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(selectCapctyUnit)).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(sbmtBtn1)).click();
			drv.waitForAngularRequestsToFinish();
			Thread.sleep(1000);
			driver.navigate().to("https://i.dev.conformity.co/super-admin/masterconnector");
			logger.log(Status.INFO, "Connector Type Added Successfully");
		}

		Thread.sleep(1000);
		lps.login4();
	}
}
package com.pages.MasterConfigs;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.Status;
import com.base.Base;
import com.config.ExcelReader;
import com.repo.pages.Global.LoginPage;

public class ChargerConfigs extends Base{

	static ExcelReader read = new ExcelReader();
	static String inputText1;
	static String inputText2;
	static String inputText3;
	static String inputText4;
	static String inputText5;
	static String chargerName = "CHCCS";
	static String delCharger = "BBAHHCCS";
	static List<WebElement> rows;
	static List<WebElement> cols;
	static String colData;
	static int rowSize;
	
	// Below are the page objects or elements to be called in test cases

	By MConfig = By.xpath("//*[text()=' Master Configuration ']");
	By ChargeConfig = By.xpath("//*[text()=' Charger Configuration ']");
	By connectConfig = By.xpath("//*[@id=\"cdk-accordion-child-3\"]/div/a[2]/div/span");
	By ChargerType = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-chargerconfiguration/mat-card[1]/form/div/div/div/input");
	By SbmtBtn1 = By.xpath("//*[text()='Submit']");
	By addMore = By.xpath("//*[text()='Add More']");
	By charCol1 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-chargerconfiguration/mat-card[2]/form/div/table/tbody/tr[1]/td[1]");
	By charCol2 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-chargerconfiguration/mat-card[2]/form/div/table/tbody/tr[2]/td[1]");
	By charCol3 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-chargerconfiguration/mat-card[2]/form/div/table/tbody/tr[3]/td[1]");
	By charCol4 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-chargerconfiguration/mat-card[2]/form/div/table/tbody/tr[4]/td[1]");
	By charCol5 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-chargerconfiguration/mat-card[2]/form/div/table/tbody/tr[5]/td[1]");

	By actionIcon = By.xpath("//app-chargerconfiguration/mat-card[2]/form/div/table/tbody/tr[5]/td[3]/app-moredetaibtn/button/span/mat-icon");
	By actionIcon1 = By.xpath("//app-chargerconfiguration/mat-card[2]/form/div/table/tbody/tr[1]/td[3]/app-moredetaibtn/button/span/mat-icon");
	By actionIcon2 = By.xpath("//app-chargerconfiguration/mat-card[2]/form/div/table/tbody/tr[2]/td[3]/app-moredetaibtn/button/span/mat-icon");
	By actionIcon3 = By.xpath("//app-chargerconfiguration/mat-card[2]/form/div/table/tbody/tr[3]/td[3]/app-moredetaibtn/button/span/mat-icon");
	By actionIcon4 = By.xpath("//app-chargerconfiguration/mat-card[2]/form/div/table/tbody/tr[4]/td[3]/app-moredetaibtn/button/span/mat-icon");
	By actionIcon5 = By.xpath("//app-chargerconfiguration/mat-card[2]/form/div/table/tbody/tr[5]/td[3]/app-moredetaibtn/button/span/mat-icon");
	By editBtn1 = By.xpath("/html/body/div[3]/div[2]/div/div/div/button[1]");

	By editBtn2 = By.xpath("//*[@id=\"cdk-overlay-1\"]/div/div/button[1]/mat-icon");
	By deleteBtn1 = By.xpath("/html/body/div[3]/div[2]/div/div/div/button[2]/span");

	By editSuccess = By.xpath("//*[text()='Success']");
	By editFailure = By.xpath("//*[text()='Failure']");
	By delsuccess = By.xpath("//*[text()='Success']");

	By cancelIcon1 = By.xpath("//app-chargerconfiguration/mat-card[2]/form/div/table/tbody/tr[1]/td[3]/div/i");
	By cancelIcon2 = By.xpath("/html/body/app-root/app-super-admin/mat-sidenav-container/mat-sidenav-content/app-chargerconfiguration/mat-card[2]/form/div/table/tbody/tr[2]/td[3]/div/i");
	By cancelIcon3 = By.xpath("//app-chargerconfiguration/mat-card[2]/form/div/table/tbody/tr[3]/td[3]/div/i");
	By cancelIcon4 = By.xpath("//app-chargerconfiguration/mat-card[2]/form/div/table/tbody/tr[4]/td[3]/div/i");
	By cancelIcon5 = By.xpath("//app-chargerconfiguration/mat-card[2]/form/div/table/tbody/tr[5]/td[3]/div/i");
	By saveBtn1 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-chargerconfiguration/mat-card[2]/form/div/table/tbody/tr[1]/td[3]/div/button");
	By saveBtn2 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-chargerconfiguration/mat-card[2]/form/div/table/tbody/tr[2]/td[3]/div/button");
	By saveBtn3 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-chargerconfiguration/mat-card[2]/form/div/table/tbody/tr[3]/td[3]/div/button");
	By saveBtn4 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-chargerconfiguration/mat-card[2]/form/div/table/tbody/tr[4]/td[3]/div/button");
	By saveBtn5 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-chargerconfiguration/mat-card[2]/form/div/table/tbody/tr[5]/td[3]/div/button");
	By confCanclBtn = By.xpath("/html/body/div[3]/div[2]/div/mat-dialog-container/app-confirmation-boxes/div/div[2]/button[1]");
	By confDelBtn = By.xpath("/html/body/div[3]/div[2]/div/mat-dialog-container/app-confirmation-boxes/div/div[2]/button[2]");
	By editInput = By.xpath("//app-chargerconfiguration/mat-card[2]/form/div/table/tbody/tr[5]/td[2]/input");
	By editInput1 = By.xpath("/html/body/app-root/app-super-admin/mat-sidenav-container/mat-sidenav-content/app-chargerconfiguration/mat-card[2]/form/div/table/tbody/tr[1]/td[2]/input");
	By editInput2 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-chargerconfiguration/mat-card[2]/form/div/table/tbody/tr[2]/td[2]/input");
	By editInput3 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-chargerconfiguration/mat-card[2]/form/div/table/tbody/tr[3]/td[2]/input");
	By editInput4 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-chargerconfiguration/mat-card[2]/form/div/table/tbody/tr[4]/td[2]/input");
	By editInput5 = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-chargerconfiguration/mat-card[2]/form/div/table/tbody/tr[5]/td[2]/input");
	By confDelBtn2 = By.xpath("//app-confirmation-boxes/div/div[2]/button[2]");
	By additionELem = By.xpath("//mat-sidenav-container/mat-sidenav-content/app-chargerconfiguration/mat-card[2]/form/div/table/thead/tr/th[2]");

// Basic test flow for charging configuration    

	public LoginPage lp  = new LoginPage();

	public void add_Edit_Delete_Charger() throws IOException, InterruptedException {

		logger.log(Status.INFO, "The Chrome browser and application is going to launch");
		Universalbrowser("chrome");
		logger.log(Status.INFO, "The user is logged in with Super Admin credentials");
		lp.login2();
		
// ====================================================    Addition of a New Charger type    ============================================

		WebDriverWait wait  = new WebDriverWait(driver, 30);
		drv.waitForAngularRequestsToFinish();
		wait.until(ExpectedConditions.elementToBeClickable(MConfig)).click();
		wait.until(ExpectedConditions.elementToBeClickable(ChargeConfig)).click();
		wait.until(ExpectedConditions.elementToBeClickable(additionELem)).click();
		Actions action  = new Actions(driver);
		action.sendKeys(Keys.PAGE_DOWN).build().perform();
		Thread.sleep(1000);
		
		List<WebElement> brows = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//tbody")));
		System.out.println(brows.size());
// ---------------------------------------  Adding the charger type if no data is available in the table ---------------------		
		if(brows.size()==1) {
			System.out.println("No data is available");
			logger.log(Status.INFO, "Adding New charger type");
			wait.until(ExpectedConditions.presenceOfElementLocated(ChargerType)).clear();
			wait.until(ExpectedConditions.presenceOfElementLocated(ChargerType)).sendKeys(ExcelReader.charger1Data());
			wait.until(ExpectedConditions.presenceOfElementLocated(SbmtBtn1)).click();
			Thread.sleep(6000);
			logger.log(Status.INFO, "Charger Type is added successfully");
			logger.log(Status.INFO, "Moving to next step of verification");
		}
		
// --------------------------- Checking the table and adding the charger type if there is some charger type is available in the table 		
		
		rows = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//mat-sidenav-container/mat-sidenav-content/app-chargerconfiguration/mat-card[2]/form/div/table/tbody/tr")));	
		rowSize = rows.size();
		System.out.println(rowSize);
		
		if(rowSize==1 && rowSize<2 && driver.findElement(editInput1).getAttribute("value").equals(chargerName)) {
			logger.log(Status.INFO, "1 One Charger Type already exists");
			action.sendKeys(Keys.PAGE_UP).build().perform();
		}
		else if(rowSize==2 && rowSize<3 && driver.findElement(editInput1).getAttribute("value").equals(chargerName) ^ driver.findElement(editInput2).getAttribute("value").equals(chargerName)) {
			logger.log(Status.INFO, "Two Charger Type already exists");
			action.sendKeys(Keys.PAGE_UP).build().perform();
		}else if(rowSize==3 && rowSize<4 && driver.findElement(editInput1).getAttribute("value").equals(chargerName) ^ driver.findElement(editInput2).getAttribute("value").equals(chargerName) ^ driver.findElement(editInput3).getAttribute("value").equals(chargerName)) {
			logger.log(Status.INFO, "Three Charger Type already exists");
			action.sendKeys(Keys.PAGE_UP).build().perform();
		}else if(rowSize==4 && rowSize<5 && driver.findElement(editInput1).getAttribute("value").equals(chargerName) ^ driver.findElement(editInput2).getAttribute("value").equals(chargerName) ^ driver.findElement(editInput3).getAttribute("value").equals(chargerName) ^ driver.findElement(editInput4).getAttribute("value").equals(chargerName)) {
			logger.log(Status.INFO, "Four Charger Type already exists");
			action.sendKeys(Keys.PAGE_UP).build().perform();
		}else if(rowSize==5 && rowSize<6 && driver.findElement(editInput1).getAttribute("value").equals(chargerName) ^ driver.findElement(editInput2).getAttribute("value").equals(chargerName) ^ driver.findElement(editInput3).getAttribute("value").equals(chargerName) ^ driver.findElement(editInput4).getAttribute("value").equals(chargerName) ^ driver.findElement(editInput5).getAttribute("value").equals(chargerName)) {
			logger.log(Status.INFO, "Five Charger Type already exists");
			action.sendKeys(Keys.PAGE_UP).build().perform();
		}

		else {	
			wait.until(ExpectedConditions.presenceOfElementLocated(ChargerType)).clear();
			wait.until(ExpectedConditions.presenceOfElementLocated(ChargerType)).sendKeys(ExcelReader.charger1Data());
			wait.until(ExpectedConditions.presenceOfElementLocated(SbmtBtn1)).click();
			Thread.sleep(3000);
			logger.log(Status.INFO, "Charger Type is added successfully");
			logger.log(Status.INFO, "Moving to next step - Edit Charger Type");
			driver.navigate().to("https://i.dev.conformity.co/super-admin/masterconfig");
			drv.waitForAngularRequestsToFinish();
			action.sendKeys(Keys.PAGE_UP).build().perform();
		}
		
		Thread.sleep(1000);
		lp.login4();  // ----------------- Logging out from the user account ----------------------------------------
		lp.login2();  // ------------------- Re-login to the user account -------------------------------------------
		
		// =========================================   Modification of the added Charger Type   ============================================
		
		driver.navigate().to("https://i.dev.conformity.co/super-admin/masterconfig");
		drv.waitForAngularRequestsToFinish();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//mat-sidenav-container/mat-sidenav/div/app-sidebar/div/div[2]/mat-nav-list/mat-accordion[4]/mat-expansion-panel/div/div/a[2]/div/span"))).click();
		drv.waitForAngularRequestsToFinish();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//mat-sidenav-container/mat-sidenav/div/app-sidebar/div/div[2]/mat-nav-list/mat-accordion[4]/mat-expansion-panel/div/div/a[1]/div/span"))).click();
		drv.waitForAngularRequestsToFinish();
		wait.until(ExpectedConditions.elementToBeClickable(additionELem)).click();
		Actions acts  = new Actions(driver);
		acts.sendKeys(Keys.PAGE_DOWN).build().perform();
		Thread.sleep(1000);		
		
//    Verifying each row of the table to check the availability of the added charger type
		
		if(rowSize==5 && driver.findElement(editInput1).getAttribute("value").equals(delCharger) ^ driver.findElement(editInput2).getAttribute("value").equals(delCharger) ^ driver.findElement(editInput3).getAttribute("value").equals(delCharger) ^ driver.findElement(editInput4).getAttribute("value").equals(delCharger) ^ driver.findElement(editInput5).getAttribute("value").equals(delCharger)) { 	
			logger.log(Status.INFO, "The data is already edited hence performing next action");	
			acts.sendKeys(Keys.PAGE_UP).build().perform();
		}
		
		else if(rowSize==4 && driver.findElement(editInput1).getAttribute("value").equals(delCharger) ^ driver.findElement(editInput2).getAttribute("value").equals(delCharger) ^ driver.findElement(editInput3).getAttribute("value").equals(delCharger) ^ driver.findElement(editInput4).getAttribute("value").equals(delCharger)) { 	
			logger.log(Status.INFO, "The data is already edited hence performing next action");	
			acts.sendKeys(Keys.PAGE_UP).build().perform();
		}
		
		else if(rowSize==3 && driver.findElement(editInput1).getAttribute("value").equals(delCharger) ^ driver.findElement(editInput2).getAttribute("value").equals(delCharger) ^ driver.findElement(editInput3).getAttribute("value").equals(delCharger)) { 	
			logger.log(Status.INFO, "The data is already edited hence performing next action");	
			acts.sendKeys(Keys.PAGE_UP).build().perform();
		}
		
		else if(rowSize==1 && rowSize<2 && driver.findElement(editInput1).getAttribute("value").equals(chargerName)) { 	
			logger.log(Status.INFO, "The data matched in record1 and is : "+driver.findElement(editInput1).getAttribute("value")+" hence further action performed");
			wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon1)).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(editBtn1)).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(editInput1)).clear();
			wait.until(ExpectedConditions.presenceOfElementLocated(editInput1)).sendKeys(ExcelReader.charger2Data());
			wait.until(ExpectedConditions.presenceOfElementLocated(saveBtn1)).click();
			Thread.sleep(1000);
			SoftAssert sast = new SoftAssert();
			sast.assertEquals(wait.until(ExpectedConditions.presenceOfElementLocated(editSuccess)).isDisplayed()==true, true);
			sast.assertAll();
			
			logger.log(Status.INFO, "The Charger is edited Successfully");
			driver.navigate().to("https://i.dev.conformity.co/super-admin/masterconfig");
			drv.waitForAngularRequestsToFinish();
			acts.sendKeys(Keys.PAGE_UP).build().perform();

		} else if(rowSize==2 && rowSize<3 && driver.findElement(editInput1).getAttribute("value").equals(chargerName) ^ driver.findElement(editInput2).getAttribute("value").equals(chargerName)) {
			logger.log(Status.INFO, "The data matched in record2 and is :"+driver.findElement(editInput2).getAttribute("value")+" hence further action performed");	 
			wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon2)).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(editBtn1)).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(editInput2)).clear();
			wait.until(ExpectedConditions.presenceOfElementLocated(editInput2)).sendKeys(ExcelReader.charger2Data());
			wait.until(ExpectedConditions.presenceOfElementLocated(saveBtn2)).click();		
			Thread.sleep(1000);
			SoftAssert sast = new SoftAssert();
			sast.assertEquals(wait.until(ExpectedConditions.presenceOfElementLocated(editSuccess)).isDisplayed()==true, true);
			sast.assertAll();
			logger.log(Status.INFO, "The Charger is edited Successfully");
			driver.navigate().to("https://i.dev.conformity.co/super-admin/masterconfig");
			drv.waitForAngularRequestsToFinish();
			acts.sendKeys(Keys.PAGE_UP).build().perform();

		}else if(rowSize==3 && rowSize<4 && driver.findElement(editInput1).getAttribute("value").equals(chargerName) ^ driver.findElement(editInput2).getAttribute("value").equals(chargerName) ^ driver.findElement(editInput3).getAttribute("value").equals(chargerName)) {
			logger.log(Status.INFO, "The data matched in record3 and is :"+driver.findElement(editInput3).getAttribute("value")+" hence further action performed");		 
			wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon3)).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(editBtn1)).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(editInput3)).clear();
			wait.until(ExpectedConditions.presenceOfElementLocated(editInput3)).sendKeys(ExcelReader.charger2Data());
			wait.until(ExpectedConditions.presenceOfElementLocated(saveBtn3)).click();	
			Thread.sleep(1000);
			SoftAssert sast = new SoftAssert();
			sast.assertEquals(wait.until(ExpectedConditions.presenceOfElementLocated(editSuccess)).isDisplayed()==true, true);
			sast.assertAll();
			logger.log(Status.INFO, "The Charger is edited Successfully");
			driver.navigate().to("https://i.dev.conformity.co/super-admin/masterconfig");
			drv.waitForAngularRequestsToFinish();
			acts.sendKeys(Keys.PAGE_UP).build().perform();

		}else if(rowSize==4 && rowSize<5 && driver.findElement(editInput1).getAttribute("value").equals(chargerName) ^ driver.findElement(editInput2).getAttribute("value").equals(chargerName) ^ driver.findElement(editInput3).getAttribute("value").equals(chargerName) ^ driver.findElement(editInput4).getAttribute("value").equals(chargerName)) {
			logger.log(Status.INFO, "The data matched in record4 and is :"+driver.findElement(editInput4).getAttribute("value")+" hence further action performed");		
			wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon4)).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(editBtn1)).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(editInput4)).clear();
			wait.until(ExpectedConditions.presenceOfElementLocated(editInput4)).sendKeys(ExcelReader.charger2Data());
			wait.until(ExpectedConditions.presenceOfElementLocated(saveBtn4)).click();
			Thread.sleep(1000);
			SoftAssert sast = new SoftAssert();
			sast.assertEquals(wait.until(ExpectedConditions.presenceOfElementLocated(editSuccess)).isDisplayed()==true, true);
			sast.assertAll();
			logger.log(Status.INFO, "The Charger is edited Successfully");
			driver.navigate().to("https://i.dev.conformity.co/super-admin/masterconfig");
			drv.waitForAngularRequestsToFinish();
			acts.sendKeys(Keys.PAGE_UP).build().perform();

		}else if(rowSize==5 && rowSize<6 && driver.findElement(editInput1).getAttribute("value").equals(chargerName) ^ driver.findElement(editInput2).getAttribute("value").equals(chargerName) ^ driver.findElement(editInput3).getAttribute("value").equals(chargerName) ^ driver.findElement(editInput4).getAttribute("value").equals(chargerName) ^ driver.findElement(editInput5).getAttribute("value").equals(chargerName)) {
			logger.log(Status.INFO, "The data matched in record5 and is :"+driver.findElement(editInput5).getAttribute("value")+" hence further action performed");		
			wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon5)).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(editBtn1)).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(editInput5)).clear();
			wait.until(ExpectedConditions.presenceOfElementLocated(editInput5)).sendKeys(ExcelReader.charger2Data());
			wait.until(ExpectedConditions.presenceOfElementLocated(saveBtn5)).click();
			Thread.sleep(1000);
			SoftAssert sast = new SoftAssert();
			sast.assertEquals(wait.until(ExpectedConditions.presenceOfElementLocated(editSuccess)).isDisplayed()==true, true);
			sast.assertAll();
			logger.log(Status.INFO, "The Charger is edited Successfully");
			driver.navigate().to("https://i.dev.conformity.co/super-admin/masterconfig");
			drv.waitForAngularRequestsToFinish();
			acts.sendKeys(Keys.PAGE_UP).build().perform();

		}else {
			logger.log(Status.INFO, "The data in record3 is: "+driver.findElement(editInput3).getAttribute("value"));	
			logger.log(Status.INFO, "The data doesn't matched with available records hence further action is not performed");
			driver.navigate().to("https://i.dev.conformity.co/super-admin/masterconfig");
			drv.waitForAngularRequestsToFinish();
			acts.sendKeys(Keys.PAGE_UP).build().perform();
		}
		
// ===============================================    Deletion of the added Charger Type  ================================================
		
		Thread.sleep(1000);
		driver.navigate().to("https://i.dev.conformity.co/super-admin/masterconfig");
		drv.waitForAngularRequestsToFinish();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//mat-sidenav-container/mat-sidenav/div/app-sidebar/div/div[2]/mat-nav-list/mat-accordion[4]/mat-expansion-panel/div/div/a[2]/div/span"))).click();
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//mat-sidenav-container/mat-sidenav/div/app-sidebar/div/div[2]/mat-nav-list/mat-accordion[4]/mat-expansion-panel/div/div/a[1]/div/span"))).click();
		drv.waitForAngularRequestsToFinish();
		wait.until(ExpectedConditions.presenceOfElementLocated(additionELem)).click();
		acts.sendKeys(Keys.PAGE_DOWN).build().perform();
		
// Verifying each row of the table to match the modified charger type and will delete after verifying the name.

		if(rowSize==1 && rowSize<2 && driver.findElement(editInput1).getAttribute("value").equals(delCharger)) { 	

			logger.log(Status.INFO, "The data matched in record 1 and further action performed");
			Thread.sleep(1000);
			wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon1)).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(deleteBtn1)).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(confCanclBtn)).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon1)).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(deleteBtn1)).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(confDelBtn)).click();
			Thread.sleep(1000);
			SoftAssert sast = new SoftAssert();
			sast.assertEquals(driver.findElement(delsuccess).isDisplayed()==true, true);
			sast.assertAll();
			logger.log(Status.INFO, "The Charger is deleted Successfully");
			driver.navigate().to("https://i.dev.conformity.co/super-admin/masterconfig");
			drv.waitForAngularRequestsToFinish();
			acts.sendKeys(Keys.PAGE_UP).build().perform();

		} else if(rowSize==2 && rowSize<3 && driver.findElement(editInput1).getAttribute("value").equals(delCharger) ^ driver.findElement(editInput2).getAttribute("value").equals(delCharger)) {
			logger.log(Status.INFO, "The data matched in record 2 and further action performed");
			Thread.sleep(1000);
			wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon2)).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(deleteBtn1)).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(confCanclBtn)).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon2)).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(deleteBtn1)).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(confDelBtn)).click();
			Thread.sleep(1000);
			SoftAssert sast = new SoftAssert();
			sast.assertEquals(driver.findElement(delsuccess).isDisplayed()==true, true);
			sast.assertAll();
			logger.log(Status.INFO, "The Charger is deleted Successfully");
			driver.navigate().to("https://i.dev.conformity.co/super-admin/masterconfig");
			drv.waitForAngularRequestsToFinish();
			acts.sendKeys(Keys.PAGE_UP).build().perform();

		}else if(rowSize==3 && rowSize<4 && driver.findElement(editInput1).getAttribute("value").equals(delCharger) ^ driver.findElement(editInput2).getAttribute("value").equals(delCharger) ^ driver.findElement(editInput3).getAttribute("value").equals(delCharger)) {

			logger.log(Status.INFO, "The data matched in record 3 and further action performed");	
			Thread.sleep(1000);
			wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon3)).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(deleteBtn1)).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(confCanclBtn)).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon3)).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(deleteBtn1)).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(confDelBtn)).click();
			Thread.sleep(1000);
			SoftAssert sast = new SoftAssert();
			sast.assertEquals(driver.findElement(delsuccess).isDisplayed()==true, true);
			sast.assertAll();
			logger.log(Status.INFO, "The Charger is deleted Successfully");
			driver.navigate().to("https://i.dev.conformity.co/super-admin/masterconfig");
			drv.waitForAngularRequestsToFinish();
			acts.sendKeys(Keys.PAGE_UP).build().perform();

		}else if(rowSize==4 && rowSize<5 && driver.findElement(editInput1).getAttribute("value").equals(delCharger) ^ driver.findElement(editInput2).getAttribute("value").equals(delCharger) ^ driver.findElement(editInput3).getAttribute("value").equals(delCharger) ^ driver.findElement(editInput4).getAttribute("value").equals(delCharger)) {

			logger.log(Status.INFO, "The data matched in record 4 and further action performed");
			Thread.sleep(1000);
			wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon4)).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(deleteBtn1)).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(confCanclBtn)).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon4)).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(deleteBtn1)).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(confDelBtn)).click();
			Thread.sleep(1000);
			SoftAssert sast = new SoftAssert();
			sast.assertEquals(driver.findElement(delsuccess).isDisplayed()==true, true);
			sast.assertAll();
			logger.log(Status.INFO, "The Charger is deleted Successfully");
			driver.navigate().to("https://i.dev.conformity.co/super-admin/masterconfig");
			drv.waitForAngularRequestsToFinish();
			acts.sendKeys(Keys.PAGE_UP).build().perform();
		}else if(rowSize==5 && rowSize<6 && driver.findElement(editInput1).getAttribute("value").equals(delCharger) ^ driver.findElement(editInput2).getAttribute("value").equals(delCharger) ^ driver.findElement(editInput3).getAttribute("value").equals(delCharger) ^ driver.findElement(editInput4).getAttribute("value").equals(delCharger) ^ driver.findElement(editInput5).getAttribute("value").equals(delCharger)) {

			logger.log(Status.INFO, "The data matched in record 5 and further action performed");
			Thread.sleep(1000);
			wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon5)).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(deleteBtn1)).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(confCanclBtn)).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(actionIcon5)).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(deleteBtn1)).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(confDelBtn)).click();
			Thread.sleep(1000);
			SoftAssert sast = new SoftAssert();
			sast.assertEquals(driver.findElement(delsuccess).isDisplayed()==true, true);
			sast.assertAll();
			logger.log(Status.INFO, "The Charger is deleted Successfully");
			driver.navigate().to("https://i.dev.conformity.co/super-admin/masterconfig");
			drv.waitForAngularRequestsToFinish();
			acts.sendKeys(Keys.PAGE_UP).build().perform();
		}else {
			logger.log(Status.INFO, "The data doesn't matched with available records hence further action is not performed");
			driver.navigate().to("https://i.dev.conformity.co/super-admin/masterconfig");
			drv.waitForAngularRequestsToFinish();
			acts.sendKeys(Keys.PAGE_UP).build().perform();
		}	
		Thread.sleep(1000);
		lp.login4(); //---------------------------  Logging out of the user account   -----------------  and quitting the selenium session
	}
}

package com.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.mail.EmailException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Protocol;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.config.TestConfigs;
import com.paulhammant.ngwebdriver.NgWebDriver;
import com.utils.TestUtils;

public class Base {
	public static ExtentReports extent;
	public static ExtentSparkReporter reporter; 
	public static ExtentTest logger;
	public static WebDriver driver;
	public static NgWebDriver drv;
	static JavascriptExecutor jsdrv;
//	static TakesScreenshot ts;
	static File source;

	static FileInputStream File;
	public static Properties prop = new Properties();

	public Base() {
		try {
			File = new FileInputStream(TestConfigs.propFilePath);
			prop.load(File);

		}catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	@BeforeTest
	public void reporter() throws IOException {
		
		reporter = new ExtentSparkReporter(System.getProperty("user.dir")+"/TestReports/AutomationTestReport.html").filter().statusFilter().as(new Status[] {Status.PASS,Status.FAIL,Status.SKIP}).apply();
//		reporter.config().setTheme(Theme.STANDARD);
        extent = new ExtentReports();
		extent.attachReporter(reporter);
		extent.setSystemInfo("HostName", "ElektriVo-I");
		extent.setSystemInfo("OS", "Windows 10");
		extent.setSystemInfo("User Name", "Mohit Baluja");
		extent.setSystemInfo("Environment", "DEV");
		reporter.config().setDocumentTitle("ElektriVo_DEV");
		reporter.config().setReportName("Automation_POC_Test_Report");
		reporter.config().setTheme(Theme.DARK);
		reporter.config().setProtocol(Protocol.HTTPS);
		reporter.config().setEncoding("UTF-8");		
	}

//	reporter.loadXMLConfig(System.getProperty("user.dir")+"/extent-config.xml");		
//	extent.loadConfig(new File(System.getProperty("user.dir")+"\\extent-config.xml"));
	
	public static void Universalbrowser(String brwsr) throws IOException, InterruptedException {

		if(brwsr.equals(prop.get("browser"))) {
//     		WebDriverManager.chromedriver().setup();
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+TestConfigs.chromeDriver);
     		driver = new ChromeDriver();
			jsdrv=(JavascriptExecutor)driver;
			drv= new NgWebDriver(jsdrv);
		}
		else if(brwsr.equals(prop.get("browser2"))){
			//	WebDriverManager.firefoxdriver().setup();
			System.setProperty("webdriver.gecko.driver", TestConfigs.ffxDriver);
			driver = new FirefoxDriver();
			jsdrv=(JavascriptExecutor)driver;
			drv= new NgWebDriver(jsdrv);	

		} 
		else if(brwsr.equals(prop.get("browser3"))){
			//	WebDriverManager.firefoxdriver().setup();
			System.setProperty("webdriver.edge.driver", TestConfigs.msedgedvr);
			driver = new EdgeDriver();
			jsdrv=(JavascriptExecutor)driver;
			drv= new NgWebDriver(jsdrv);	

		} 
		else {
			System.out.println("Browser is not available");
		}

		driver.manage().window().maximize();
		driver.get(TestConfigs.app_url);
		Thread.sleep(5000);
		drv.waitForAngularRequestsToFinish();
		driver.manage().timeouts().pageLoadTimeout(TestUtils.PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(TestUtils.IMPLICIT_WAIT, TimeUnit.SECONDS);
		driver.manage().deleteAllCookies();	
	}

	@AfterTest
	public void generateReport() throws EmailException {
	extent.flush();
//	TestUtils.sendMail();
	}
}

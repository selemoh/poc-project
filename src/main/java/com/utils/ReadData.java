package com.utils;

import java.io.File;
import java.io.FileInputStream;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadData {

	public static FileInputStream iStream;
	public static XSSFWorkbook Testbook;
//	public static String fileExtentionName;
	public static File file;
	public static XSSFSheet testSheet;
	public static XSSFRow row;
	public static XSSFCell cell;
	public static DataFormatter formatter = new DataFormatter();
	
	public static Object[][] readExcelData(String sheetName) {
		try {
			file = new File("E:\\Automation\\com.poc.elektrivo\\TestData\\TestData_1.xlsx"); 
			iStream = new FileInputStream(file);
		    Testbook = new XSSFWorkbook(iStream);
//		    int rowCount = testSheet.getPhysicalNumberOfRows();
//		    row = testSheet.getRow(0);
//		    cell = row.getCell(0);	
			}catch(Exception exp) {
				System.out.println(exp.getMessage());
				System.out.println(exp.getCause());
				exp.printStackTrace();
			}
		 testSheet = Testbook.getSheetAt(0);
		 int rowCount = testSheet.getPhysicalNumberOfRows();
		 row = testSheet.getRow(0);
		 int colCount = testSheet.getLastRowNum();
	
		 Object data[][] = new Object[rowCount-1][colCount];
		 
		 for(int i=0; i<rowCount; i++) {
			 row = testSheet.getRow(i);
			 for(int j=0; j<colCount; j++) {
				 cell = row.getCell(j);
				 data[i][j]=formatter.formatCellValue(cell);
			 }
			 
		 }
		 return data;
	}	
	
}

/*
 * fileExtentionName = fileName.substring(fileName.indexOf("."));
 * 
 * if(fileExtentionName.equals(".xlsx")) { Testbook = new XSSFWorkbook(iStream);
 * }else if(fileExtentionName.equals(".xls")) { Testbook = new
 * HSSFWorkbook(iStream); }
 * 
 * testSheet = Testbook.getSheet(sheetName);
 */

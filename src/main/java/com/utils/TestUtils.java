package com.utils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.config.TestConfigs;

public class TestUtils {

	public static long PAGE_LOAD_TIMEOUT=20;
	public static long IMPLICIT_WAIT=20;
	static TakesScreenshot ts;
	static File source;	
	static File finaldest;
	static ReadFile reading;
	
	public static String getScreenshot(WebDriver driver, String ScreenshotName) throws IOException {
		String date1 = new SimpleDateFormat("_dd_MMM_yyyy_hh_mm_ss").format(new Date());
		ts = (TakesScreenshot)driver;
		source = ts.getScreenshotAs(OutputType.FILE);
		String dest = System.getProperty("user.dir")+"/TestReports/" + ScreenshotName + date1+".png";
		finaldest = new File(dest);
		FileUtils.copyFile(source, finaldest);
		return dest;
	}  
	
	public static void sendMail () throws EmailException {
		
		  EmailAttachment attachment = new EmailAttachment();
		  attachment.setPath(System.getProperty("user.dir")+"\\TestReports\\AutomationTestReport.html");
		  attachment.setDisposition(EmailAttachment.ATTACHMENT);
		  attachment.setDescription("Detailed Automation test Report");
		  attachment.setName("AutomationTestReport.html");
		
		HtmlEmail email = new HtmlEmail();
		email.setHostName("smtp.gmail.com");
		email.setSmtpPort(465);
		email.setAuthenticator(new DefaultAuthenticator("baluja.mohit7@gmail.com", "baluja@1983"));
		email.setSSLOnConnect(true);
		email.setFrom("baluja.mohit7@gmail.com");
		email.setSubject("Selenium POC Test Report");
		email.setMsg("Hello User,\nThis is a test report of selenium test execution on ElektriVo Web Application.\nPlease check the attachment for the details of test execution.\n\n\nThanks & Regards,\nMohit Baluja");
//		email.addTo("mohitbaluja2013@gmail.com");
		email.addTo("mohit@interwork.biz");
//		email.addTo("akanshu@interwork.biz");
//		email.addTo("niharika@interwork.biz");
		email.attach(attachment);
		email.send();
		
	}
	
	public static ArrayList<Object[]> getDatafrmExl(){
		ArrayList<Object[]> myData = new ArrayList<Object[]>();
		try {
			reading = new ReadFile(TestConfigs.nextTestData);	
		}
		catch(Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		for (int rowNum=2; rowNum<=reading.getRowCount("Sheet1"); rowNum++) {
			
			String usermail = reading.getCellData("Sheet1", "username", rowNum);
			String userpass = reading.getCellData("Sheet1", "password", rowNum).toString();
			
			Object ob[] = {usermail, userpass};
			myData.add(ob);
		}
		return myData;
	}
	
	
	public static ArrayList<Object[]> getResetPassData(){
		ArrayList<Object[]> myData = new ArrayList<Object[]>();
		try {
			reading = new ReadFile(TestConfigs.nextTestData);	
		}
		catch(Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		for (int rowNum=2; rowNum<=reading.getRowCount("Sheet2"); rowNum++) {
			
			String setMail = reading.getCellData("Sheet2", "resetMail", rowNum);
			
			Object ob[] = {setMail};
			myData.add(ob);
		}
		return myData;
	}
	
	
	
	/*
	 * @DataProvider public Object [][] excelDP() throws IOException { return new
	 * Object[][]{{"spradmnw@mailinator.com","1234"}}; }
	 */
}

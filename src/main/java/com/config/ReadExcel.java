package com.config;

import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.DataProvider;

import com.base.Base;

public class ReadExcel extends Base{
	
	static XSSFWorkbook wb;
	static XSSFSheet sh;
	
	@DataProvider
	public Object [][] excelData1() throws IOException {
		Object arrDp[][] = getExcelData(0);
		return arrDp;
	}
	
	public Object[][] getExcelData(int sheetNum) throws IOException {

		FileInputStream fis = new FileInputStream(TestConfigs.nextTestData);
		XSSFWorkbook wb = new XSSFWorkbook(fis);
		XSSFSheet sh = wb.getSheetAt(sheetNum);
		
		Object [][] data = new Object [sh.getLastRowNum()][sh.getRow(0).getLastCellNum()];

		for(int i =1; i<sh.getLastRowNum();i++){
			for(int j=0;j<sh.getRow(0).getLastCellNum();j++){
				data [i][j]= sh.getRow(0).getCell(j).toString();
			}
		}
		wb.close();
		return data;
	}
}

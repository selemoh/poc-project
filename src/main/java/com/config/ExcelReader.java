package com.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelReader {
	
	static File file;
	static FileInputStream fis;
	static XSSFWorkbook wb;
	static XSSFSheet sheets;
	static XSSFSheet sheets1;
	static XSSFSheet sheets2;
	static String charger;
	static String charger1;
	static String connectorType;
	static String connectorType1;
	static String chargingCap;
	static String chargingCap1;
	static String connectCaps;
	static String connectCap2;
	
	public ExcelReader() {
		try{
			file = new File(TestConfigs.secondtestData);
		fis = new FileInputStream(file);
		wb = new XSSFWorkbook(fis);
		}catch(IOException io) {
			io.printStackTrace();
			System.out.println(io.getCause());
		}
	   sheets = wb.getSheetAt(0);
	   sheets1 = wb.getSheetAt(1);
	   sheets2 = wb.getSheetAt(1);
	}
		
	public static String charger1Data() throws IOException {
			
		   for(int i=0; i<=sheets.getLastRowNum(); i++) {
		   charger  = sheets.getRow(i).getCell(i).getStringCellValue();
		   }
		return charger;
		
	}	
	
	public static String charger2Data() throws IOException {
		
		   for(int i=0; i<=sheets.getLastRowNum(); i++) {
		   charger  = sheets.getRow(i).getCell(i+1).getStringCellValue();
		   }
		return charger;
		
	}
	
	public static String connector1() throws IOException {
		   for(int j=0; j<=sheets1.getLastRowNum(); j++) {
		   connectorType  = sheets1.getRow(0).getCell(0).toString();		   
		   }
		return connectorType;
	}
	
	public static String connectorCap1() throws IOException {
		
		   for(int k=0; k<=sheets1.getLastRowNum(); k++) {
			   connectCaps  = sheets1.getRow(0).getCell(1).getRawValue();		   
		   }
		return connectCaps;
	}
	
	public static String connector2() throws IOException {
		   for(int i=0; i<=sheets1.getLastRowNum(); i++) {
			   connectorType1 = sheets1.getRow(1).getCell(0).toString();
			   
		   }
		return connectorType1;
	}
	public static String connectorCap2() throws IOException {
		
		   for(int i=1; i<=sheets1.getLastRowNum(); i++) {
			   connectCap2  = sheets1.getRow(1).getCell(1).getRawValue();		   
		   }
		return connectCap2;
	}
		
	public static String capacity1Data() throws IOException {
		
		   for(int i=0; i<=sheets1.getLastRowNum(); i++) {
			   chargingCap  = sheets1.getRow(i).getCell(i+1).toString();		   
		   }
		return chargingCap;
	}
	
	public static String capacity2Data() throws IOException {
		   for(int i=0; i<=sheets1.getLastRowNum(); i++) {
			   chargingCap1 = sheets1.getRow(i+1).getCell(i+1).toString();
		   }
		return chargingCap1;
	}
}